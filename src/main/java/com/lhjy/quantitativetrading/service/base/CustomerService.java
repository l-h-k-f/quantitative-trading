package com.lhjy.quantitativetrading.service.base;

import com.github.pagehelper.PageInfo;
import com.lhjy.quantitativetrading.dal.dto.base.idto.AddCustomerIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.idto.QueryCustomerIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.idto.UpdateCustomerIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.odto.CustomerInfoODTO;

/**
 * @ClassName CustomerService
 * @Description 客户信息
 * @Author wangdehonga
 * @Date 2020/11/2 17:39
 * @Version 1.0
 */
public interface CustomerService {

    /**
     * 新增客户信息
     * @param addCustomerIDTO 新增客户入参实体
     */
    void insertCustomer(AddCustomerIDTO addCustomerIDTO);

    /**
     * 修改客户信息
     * @param updateCustomerIDTO 修改客户入参实体
     */
    void updateCustomer(UpdateCustomerIDTO updateCustomerIDTO);

    /**
     * 删除客户信息
     * @param id 主键
     */
    void deleteCus(Integer id);

    /**
     * 查询客户信息
     * @param queryCustomerIDTO 查询客户入参实体
     * @return PageInfo<CustomerInfoODTO>
     */
    PageInfo<CustomerInfoODTO> queryCusInfo(QueryCustomerIDTO queryCustomerIDTO);
}
