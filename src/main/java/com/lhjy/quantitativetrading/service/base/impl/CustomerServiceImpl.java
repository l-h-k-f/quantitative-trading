package com.lhjy.quantitativetrading.service.base.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhjy.quantitativetrading.dal.constant.CommonNounsConstant;
import com.lhjy.quantitativetrading.dal.dao.TCustomerInfoDAO;
import com.lhjy.quantitativetrading.dal.dto.base.idto.AddCustomerIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.idto.QueryCustomerIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.idto.UpdateCustomerIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.odto.CustomerInfoODTO;
import com.lhjy.quantitativetrading.dal.entity.TCustomerInfo;
import com.lhjy.quantitativetrading.service.base.CustomerService;
import com.lhjy.quantitativetrading.utils.common.MyException;
import com.lhjy.quantitativetrading.utils.common.RandomUtils;
import com.lhjy.quantitativetrading.utils.common.ReturnMsg;
import com.lhjy.quantitativetrading.utils.thread.AgentThreadLocal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @ClassName CustomerServiceImpl
 * @Description
 * @Author wangdehonga
 * @Date 2020/11/2 17:40
 * @Version 1.0
 */
@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private TCustomerInfoDAO tCustomerInfoDAO;

    @Override
    public void insertCustomer(AddCustomerIDTO addCustomerIDTO) {
        TCustomerInfo record = new TCustomerInfo();
        record.setCustomerId(CommonNounsConstant.CUS_TAB + RandomUtils.randomNumbers(6));
        record.setCustomerName(addCustomerIDTO.getCustomerName());
        record.setCreateor(AgentThreadLocal.get().getUserId());
        Optional.of(tCustomerInfoDAO.insertSelective(record) > 0).orElseThrow(() -> new MyException(ReturnMsg.CUSTOMER_BASIC_INFORMATION_STORAGE_FAILED));
    }

    @Override
    public void updateCustomer(UpdateCustomerIDTO updateCustomerIDTO) {
        TCustomerInfo record = new TCustomerInfo();
        record.setId(updateCustomerIDTO.getId());
        record.setCustomerName(updateCustomerIDTO.getCustomerName());
        Optional.of(tCustomerInfoDAO.updateByPrimaryKeySelective(record) > 0).orElseThrow(() -> new MyException(ReturnMsg.CUSTOMER_BASIC_INFORMATION_STORAGE_FAILED));
    }

    @Override
    public void deleteCus(Integer id) {
        Optional.of(tCustomerInfoDAO.deleteByPrimaryKey(id) > 0).orElseThrow(() -> new MyException(ReturnMsg.CUSTOMER_BASIC_INFORMATION_UPDATE_FAILED));
    }

    @Override
    public PageInfo<CustomerInfoODTO> queryCusInfo(QueryCustomerIDTO queryCustomerIDTO) {
        PageHelper.startPage(queryCustomerIDTO.getPageNum(), queryCustomerIDTO.getPageSize());
        List<CustomerInfoODTO> cusInfoList = tCustomerInfoDAO.selectCusListByIdAndName(queryCustomerIDTO);
        return new PageInfo<>(cusInfoList);
    }
}
