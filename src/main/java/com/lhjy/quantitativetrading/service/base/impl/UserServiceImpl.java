package com.lhjy.quantitativetrading.service.base.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhjy.quantitativetrading.dal.constant.CommonNounsConstant;
import com.lhjy.quantitativetrading.dal.dao.UserInfoDAO;
import com.lhjy.quantitativetrading.dal.dto.base.idto.QueryUserIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.idto.UserAddIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.idto.UserEditIDTO;
import com.lhjy.quantitativetrading.dal.entity.UserInfo;
import com.lhjy.quantitativetrading.service.base.UserService;
import com.lhjy.quantitativetrading.utils.common.MyException;
import com.lhjy.quantitativetrading.utils.common.RandomUtils;
import com.lhjy.quantitativetrading.utils.common.ReturnMsg;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

/**
 * @ClassName UserServiceImpl
 * @Description
 * @Author wangdehonga
 * @Date 2020/11/2 17:40
 * @Version 1.0
 */
public class UserServiceImpl implements UserService {

    @Autowired
    private UserInfoDAO userInfoDAO;

    @Override
    public void insertUserInfo(UserAddIDTO userAddIDTO) {
        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(CommonNounsConstant.USER_TAB + RandomUtils.randomNumbers(6));
        userInfo.setUserAccount(userAddIDTO.getUserAccount());
        userInfo.setUserPassword(Optional.ofNullable(userAddIDTO.getUserPassword()).orElse(null));
        Optional.of(userInfoDAO.insertSelective(userInfo) > 0).orElseThrow(() -> new MyException(ReturnMsg.USER_BASIC_INFORMATION_STORAGE_FAILED));
    }

    @Override
    public void updateUserInfo(UserEditIDTO userEditIDTO) {
        Optional.of(userInfoDAO.updateSelective(userEditIDTO) > 0).orElseThrow(() -> new MyException(ReturnMsg.USER_UPDATE_FAILED));
    }

    @Override
    public void deleteUser(String userId) {
        Optional.ofNullable(userId).orElseThrow(() -> new MyException(ReturnMsg.ENTRY_PARAMETER_ISEMPTY));
        Optional.of(userInfoDAO.deleteByPrimaryKey(userId) > 0).orElseThrow(() -> new MyException(ReturnMsg.USER_DELETE_FAILED));
    }

    @Override
    public PageInfo<UserInfo> selectUsers(QueryUserIDTO queryUserIDTO) {
        PageHelper.startPage(queryUserIDTO.getPageNum(), queryUserIDTO.getPageSize());
        List<UserInfo> userInfoList = userInfoDAO.selectUsers(queryUserIDTO);
        return new PageInfo<>(userInfoList);
    }
}
