package com.lhjy.quantitativetrading.service.base;

import com.lhjy.quantitativetrading.dal.dto.base.idto.*;
import com.lhjy.quantitativetrading.dal.dto.base.odto.UserInfoODTO;

/**
 * @ClassName UserOperationService
 * @Description 用户相关操作
 * @Author wangdehonga
 * @Date 2020/10/28 13:34
 * @Version 1.0
 */
public interface UserOperationService {

    /**
     * 用户登录
     * @param signInFormIDTO 登录请求表单
     * @return UserInfo
     */
    UserInfoODTO checkUserInfoforSignIn(SignInFormIDTO signInFormIDTO);
}
