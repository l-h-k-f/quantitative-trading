package com.lhjy.quantitativetrading.service.base.impl;

import com.github.pagehelper.PageHelper;
import com.lhjy.quantitativetrading.dal.dao.TCustomerInfoDAO;
import com.lhjy.quantitativetrading.dal.dao.UserInfoDAO;
import com.lhjy.quantitativetrading.dal.dto.account.idto.QueryAccountIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.idto.*;
import com.lhjy.quantitativetrading.dal.dto.base.odto.AccountByCreateorODTO;
import com.lhjy.quantitativetrading.dal.entity.TCustomerInfo;
import com.lhjy.quantitativetrading.dal.entity.UserInfo;
import com.lhjy.quantitativetrading.utils.thread.AgentThreadLocal;
import com.github.pagehelper.PageInfo;
import com.lhjy.quantitativetrading.dal.dao.TApiAccountDAO;
import com.lhjy.quantitativetrading.dal.dto.base.odto.AccountByOwnerODTO;
import com.lhjy.quantitativetrading.dal.dto.base.odto.AccountOwnInfoODTO;
import com.lhjy.quantitativetrading.dal.entity.TApiAccount;
import com.lhjy.quantitativetrading.service.base.ExchangeService;
import com.lhjy.quantitativetrading.utils.common.MyException;
import com.lhjy.quantitativetrading.utils.common.ReturnMsg;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @ClassName ExchangeServiceImpl
 * @Description
 * @Author wangdehonga
 * @Date 2020/11/2 17:40
 * @Version 1.0
 */
@Service
@Slf4j
public class ExchangeServiceImpl implements ExchangeService {

    @Autowired
    private TApiAccountDAO tApiAccountDAO;
    @Autowired
    private TCustomerInfoDAO tCustomerInfoDAO;
    @Autowired
    private UserInfoDAO userInfoDAO;

    @Override
    public void insertAccount(AccountAddIDTO accountAddIDTO) {
        checkPromiss(accountAddIDTO.getOwnerId());
        TApiAccount record = new TApiAccount();
        record.setExchangeId(accountAddIDTO.getExchangeId());
        record.setApiKey(accountAddIDTO.getApiKey());
        record.setSecretKey(accountAddIDTO.getSecretKey());
        record.setPassPhrase(Optional.ofNullable(accountAddIDTO.getPassPhrase()).orElse(""));
        record.setTag(accountAddIDTO.getTag());
        record.setOwnerId(accountAddIDTO.getOwnerId());
        Optional.of(tApiAccountDAO.insertSelective(record) > 0).orElseThrow(() -> new MyException(ReturnMsg.ACCOUNT_INSERT_FAILED));
    }

    @Override
    public void updateAccount(AccountEditIDTO accountEditIDTO) {
        checkPromiss(accountEditIDTO.getId());
        TApiAccount record = new TApiAccount();
        record.setId(accountEditIDTO.getId());
        record.setTag(Optional.ofNullable(accountEditIDTO.getTag()).orElse(null));
        Optional.of(tApiAccountDAO.updateByPrimaryKeySelective(record) > 0).orElseThrow(() -> new MyException(ReturnMsg.ACCOUNT_INSERT_FAILED));
    }

    @Override
    public void deleteAccount(Integer id) {
        checkPromiss(id);
        Optional.ofNullable(id).orElseThrow(() -> new MyException(ReturnMsg.ENTRY_PARAMETER_ISEMPTY));
        Optional.of(tApiAccountDAO.deleteByPrimaryKey(id) > 0).orElseThrow(() -> new MyException(ReturnMsg.ACCOUNT_DELETE_FAILED));
    }

    @Override
    public PageInfo<AccountOwnInfoODTO> selectOwnAccounts(QueryOwnAccountIDTO queryOwnAccountIDTO) {
        QueryAccountIDTO queryAccountIDTO = new QueryAccountIDTO();
        queryAccountIDTO.setOwnerId(AgentThreadLocal.get().getUserId());
        List<AccountOwnInfoODTO> accountList = new ArrayList<>();

        PageHelper.startPage(queryOwnAccountIDTO.getPageNum(), queryOwnAccountIDTO.getPageSize());
        List<TApiAccount> tApiAccountList = tApiAccountDAO.selectAccount(queryAccountIDTO);
        tApiAccountList.forEach( tApiAccount -> {
            accountList.add(tApiAccountToAccountOwnInfoODTO(tApiAccount));
        });

        return new PageInfo<>(accountList);
    }

    @Override
    public List<AccountByOwnerODTO> selectCustomerAccounts(QueryCustomerAccountIDTO queryCustomerAccountIDTO) {
        List<AccountByOwnerODTO> accountByOwnerODTOList = new ArrayList<>();
        //查询客户列表
        PageHelper.startPage(queryCustomerAccountIDTO.getPageNum(), queryCustomerAccountIDTO.getPageSize());
        List<TCustomerInfo> tCustomerInfoList = tCustomerInfoDAO.selectCusList(AgentThreadLocal.get().getUserId(), queryCustomerAccountIDTO.getCustomerName());
        //查询客户的账户列表
        Optional.ofNullable(tCustomerInfoList).orElseThrow(() -> new MyException(ReturnMsg.CUSTOMER_INFORMATION_IS_EMPTY));
        tCustomerInfoList.forEach(customer -> {
            AccountByOwnerODTO accountByOwnerODTO = new AccountByOwnerODTO();
            QueryAccountIDTO queryAccountIDTO = new QueryAccountIDTO();
            queryAccountIDTO.setOwnerId(customer.getCustomerId());
            queryAccountIDTO.setExchangeId(queryCustomerAccountIDTO.getExchangeId());
            queryAccountIDTO.setTag(queryCustomerAccountIDTO.getTag());
            List<TApiAccount> tApiAccountList = tApiAccountDAO.selectAccount(queryAccountIDTO);
            List<AccountOwnInfoODTO> accountOwnInfoODTOList = new ArrayList<>();
            if(CollectionUtils.isEmpty(tApiAccountList)) {
                accountByOwnerODTO.setAccountOwnInfoODTOList(null);
            } else {
                tApiAccountList.forEach(account -> {
                    AccountOwnInfoODTO accountOwnInfoODTO = new AccountOwnInfoODTO();
                    BeanUtils.copyProperties(account, accountOwnInfoODTO);
                    accountOwnInfoODTOList.add(accountOwnInfoODTO);
                });
                accountByOwnerODTO.setAccountOwnInfoODTOList(accountOwnInfoODTOList);
            }
            accountByOwnerODTO.setId(customer.getId());
            accountByOwnerODTO.setCustomerId(customer.getCustomerId());
            accountByOwnerODTO.setCustomerName(customer.getCustomerName());
            accountByOwnerODTOList.add(accountByOwnerODTO);
        });
        return accountByOwnerODTOList;
    }

    @Override
    public List<AccountByCreateorODTO> selectCustomerAccountsForUser(QueryUserAccountIDTO queryUserAccountIDTO) {
        //权限鉴别，管理员才能查看用户信息
        if(AgentThreadLocal.get().getUserRole().equals(2)) {
            throw new MyException(ReturnMsg.NO_ACCESS_RIGHTS);
        }
        List<AccountByCreateorODTO> accountByCreateorODTOList = new ArrayList<>();
        //查询用户列表
        QueryUserIDTO queryUserIDTO = new QueryUserIDTO();
        if(!StringUtils.isEmpty(queryUserAccountIDTO.getUserId())) {
            queryUserIDTO.setUserId(queryUserAccountIDTO.getUserId());
        }
        PageHelper.startPage(queryUserAccountIDTO.getPageNum(), queryUserAccountIDTO.getPageSize());
        List<UserInfo> userList = userInfoDAO.selectUsers(queryUserIDTO);
        //查询客户列表
        if(CollectionUtils.isEmpty(userList)) {
            throw new MyException(ReturnMsg.USER_INFO_IS_EMPTY);
        }
        userList.forEach(user -> {
            AccountByCreateorODTO accountByCreateorODTO = new AccountByCreateorODTO();
            List<TCustomerInfo> tCustomerInfoList = tCustomerInfoDAO.selectCusList(user.getUserId(), queryUserAccountIDTO.getCustomerName());
            //查询客户的账户列表
            Optional.ofNullable(tCustomerInfoList).orElseThrow(() -> new MyException(ReturnMsg.CUSTOMER_INFORMATION_IS_EMPTY));
            List<AccountByOwnerODTO> accountByOwnerODTOList = new ArrayList<>();
            tCustomerInfoList.forEach(customer -> {
                AccountByOwnerODTO accountByOwnerODTO = new AccountByOwnerODTO();
                QueryAccountIDTO queryAccountIDTO = new QueryAccountIDTO();
                queryAccountIDTO.setOwnerId(customer.getCustomerId());
                queryAccountIDTO.setExchangeId(queryUserAccountIDTO.getExchangeId());
                queryAccountIDTO.setTag(queryUserAccountIDTO.getTag());
                List<TApiAccount> tApiAccountList = tApiAccountDAO.selectAccount(queryAccountIDTO);
                List<AccountOwnInfoODTO> accountOwnInfoODTOList = new ArrayList<>();
                if(CollectionUtils.isEmpty(tApiAccountList)) {
                    accountByOwnerODTO.setAccountOwnInfoODTOList(null);
                } else {
                    tApiAccountList.forEach(account -> {
                        AccountOwnInfoODTO accountOwnInfoODTO = new AccountOwnInfoODTO();
                        BeanUtils.copyProperties(account, accountOwnInfoODTO);
                        accountOwnInfoODTOList.add(accountOwnInfoODTO);
                    });
                    accountByOwnerODTO.setAccountOwnInfoODTOList(accountOwnInfoODTOList);
                }
                accountByOwnerODTO.setId(customer.getId());
                accountByOwnerODTO.setCustomerId(customer.getCustomerId());
                accountByOwnerODTO.setCustomerName(customer.getCustomerName());
                accountByOwnerODTOList.add(accountByOwnerODTO);
            });

            accountByCreateorODTO.setId(user.getId());
            accountByCreateorODTO.setUserId(user.getUserId());
            accountByCreateorODTO.setUserAccount(user.getUserAccount());
            accountByCreateorODTO.setUserRole(user.getUserRole());
            accountByCreateorODTO.setAccountByOwnerODTOList(accountByOwnerODTOList);
            accountByCreateorODTOList.add(accountByCreateorODTO);
        });
        return accountByCreateorODTOList;
    }
    private void checkPromiss(String ownerId) {
        if (!AgentThreadLocal.get().getUserId().equals(ownerId)) {
            //查看该用户有没有权限给此顾客更新信息，没有则抛异常
            if(tCustomerInfoDAO.checkAccountForUserId(AgentThreadLocal.get().getUserId(), ownerId) == 0) {
                throw new MyException(ReturnMsg.NO_PERMISSION_TO_ADD);
            }
        }
    }

    private void checkPromiss(Integer id) {
        //查看该用户有没有权限给此顾客更新信息，没有则抛异常
        if(tCustomerInfoDAO.checkAccountForId(AgentThreadLocal.get().getUserId(), id) == 0) {
            throw new MyException(ReturnMsg.NO_PERMISSION_TO_ADD);
        }
    }

    private AccountOwnInfoODTO tApiAccountToAccountOwnInfoODTO(TApiAccount tApiAccount) {
        AccountOwnInfoODTO accountOwnInfoODTO = new AccountOwnInfoODTO();
        accountOwnInfoODTO.setId(tApiAccount.getId());
        accountOwnInfoODTO.setExchangeName(Integer.valueOf(tApiAccount.getExchangeId()));
        accountOwnInfoODTO.setApiKey(tApiAccount.getApiKey());
        accountOwnInfoODTO.setSecretKey(tApiAccount.getSecretKey());
        accountOwnInfoODTO.setPassPhrase(tApiAccount.getPassPhrase());
        accountOwnInfoODTO.setTag(tApiAccount.getTag());
        return accountOwnInfoODTO;
    }
}
