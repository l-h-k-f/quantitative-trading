package com.lhjy.quantitativetrading.service.base.impl;

import com.lhjy.quantitativetrading.dal.dao.UserInfoDAO;
import com.lhjy.quantitativetrading.dal.dto.base.idto.*;
import com.lhjy.quantitativetrading.dal.dto.base.odto.UserInfoODTO;
import com.lhjy.quantitativetrading.dal.entity.UserInfo;
import com.lhjy.quantitativetrading.service.base.UserOperationService;
import com.lhjy.quantitativetrading.utils.common.MyException;
import com.lhjy.quantitativetrading.utils.common.ReturnMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

/**
 * @ClassName UserOperationServiceImpl
 * @Description
 * @Author wangdehonga
 * @Date 2020/10/28 13:43
 * @Version 1.0
 */
@Slf4j
@Service
public class UserOperationServiceImpl implements UserOperationService {

    @Autowired
    private UserInfoDAO userInfoDAO;

    @Override
    public UserInfoODTO checkUserInfoforSignIn(SignInFormIDTO signInFormIDTO) {
        UserInfo userInfo = Optional.ofNullable(userInfoDAO.selectByUserNumAndPws(signInFormIDTO)).orElseThrow(() -> new MyException(ReturnMsg.USER_INFORMATION_EMPTY));
        UserInfoODTO userInfoODTO = new UserInfoODTO();
        BeanUtils.copyProperties(userInfo, userInfoODTO);
        userInfoODTO.setToken(UUID.randomUUID().toString().replaceAll("-", ""));
        return userInfoODTO;
    }
}
