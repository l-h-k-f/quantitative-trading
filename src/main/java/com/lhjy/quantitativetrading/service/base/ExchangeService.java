package com.lhjy.quantitativetrading.service.base;

import com.github.pagehelper.PageInfo;
import com.lhjy.quantitativetrading.dal.dto.base.idto.*;
import com.lhjy.quantitativetrading.dal.dto.base.odto.AccountByCreateorODTO;
import com.lhjy.quantitativetrading.dal.dto.base.odto.AccountByOwnerODTO;
import com.lhjy.quantitativetrading.dal.dto.base.odto.AccountOwnInfoODTO;

import java.util.List;

/**
 * @ClassName ExchangeService
 * @Description 第三方账户信息
 * @Author wangdehonga
 * @Date 2020/11/2 17:38
 * @Version 1.0
 */
public interface ExchangeService {

    /**
     * 添加账户
     * @param accountAddIDTO 第三方账户信息新增入参实体
     * @return void
     */
    void insertAccount(AccountAddIDTO accountAddIDTO);

    /**
     * 编辑账户
     * @param accountEditIDTO 第三方账户信息修改入参实体
     * @return void
     */
    void updateAccount(AccountEditIDTO accountEditIDTO);

    /**
     * 删除账户
     * @param id 第三方账户信息主键
     * @return void
     */
    void deleteAccount(Integer id);

    /**
     * 查询自己账户信息
     * @param queryOwnAccountIDTO 第三方账户信息查询自己入参实体
     * @return List<UserODTO>
     */
    PageInfo<AccountOwnInfoODTO> selectOwnAccounts(QueryOwnAccountIDTO queryOwnAccountIDTO);

    /**
     * 查询客户账户信息
     * @param queryCustomerAccountIDTO 第三方账户信息查询客户入参实体
     * @return List<AccountByCreateorODTO>
     */
    List<AccountByOwnerODTO> selectCustomerAccounts(QueryCustomerAccountIDTO queryCustomerAccountIDTO);


    /**
     * 查询用户账户信息
     * @param queryUserAccountIDTO 第三方账户信息查询用户入参实体
     * @return List<AccountByCreateorODTO>
     */
    List<AccountByCreateorODTO> selectCustomerAccountsForUser(QueryUserAccountIDTO queryUserAccountIDTO);


}
