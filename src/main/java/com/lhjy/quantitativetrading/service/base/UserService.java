package com.lhjy.quantitativetrading.service.base;

import com.github.pagehelper.PageInfo;
import com.lhjy.quantitativetrading.dal.dto.base.idto.QueryUserIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.idto.UserAddIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.idto.UserEditIDTO;
import com.lhjy.quantitativetrading.dal.entity.UserInfo;

/**
 * @ClassName UserService
 * @Description 平台用户信息
 * @Author wangdehonga
 * @Date 2020/11/2 17:36
 * @Version 1.0
 */
public interface UserService {

    /**
     * 添加用户
     * @param userAddIDTO 增加用户信息入参
     * @return void
     */
    void insertUserInfo(UserAddIDTO userAddIDTO);

    /**
     * 修改用户
     * @param userEditIDTO 修改用户信息入参
     * @return void
     */
    void updateUserInfo(UserEditIDTO userEditIDTO);

    /**
     * 删除用户
     * @param userId 用户id
     * @return void
     */
    void deleteUser(String userId);

    /**
     * 查询用户信息
     * @param queryUserIDTO 用户信息查询入参实体
     * @return List<UserODTO>
     */
    PageInfo<UserInfo> selectUsers(QueryUserIDTO queryUserIDTO);
}
