package com.lhjy.quantitativetrading.dal.entity;

import lombok.Data;

/**
 * t_api_account
 * @author 
 */
@Data
public class TApiAccount{
    private Integer id;

    /**
     * 账户持有者id
     */
    private String ownerId;

    /**
     * 交易所编号
     */
    private String exchangeId;

    /**
     * apikey
     */
    private String apiKey;

    /**
     * 密钥
     */
    private String secretKey;

    /**
     * okex的pass_phrase和火币的UID保留字段
     */
    private String passPhrase;

    /**
     * 操盘标志，1：实盘；2：模拟盘
     */
    private Integer tag;
}