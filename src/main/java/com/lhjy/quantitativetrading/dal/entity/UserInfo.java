package com.lhjy.quantitativetrading.dal.entity;

import lombok.Data;

/**
 * t_user_info
 * @author 
 */
@Data
public class UserInfo{

    private Integer id;

    /**
     * 用户id，随机生成且唯一
     */
    private String userId;

    /**
     * 用户账号
     */
    private String userAccount;

    /**
     * 用户密码
     */
    private String userPassword;

    /**
     * 角色：1：管理员（可添加用户）2：用户（仅可添加交易所账号信息）
     */
    private Integer userRole;
}