package com.lhjy.quantitativetrading.dal.entity;

import lombok.Data;

/**
 * t_customer_info
 * @author 
 */
@Data
public class TCustomerInfo{
    private Integer id;

    /**
     * 客户id，随机生成且唯一
     */
    private String customerId;

    /**
     * 客户称谓
     */
    private String customerName;

    /**
     * 添加人
     */
    private String createor;
}