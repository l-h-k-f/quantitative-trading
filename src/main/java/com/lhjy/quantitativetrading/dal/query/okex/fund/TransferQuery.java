package com.lhjy.quantitativetrading.dal.query.okex.fund;

import lombok.Data;

/**
 * @ClassName TransferQuery
 * @Description 资金划转
 * @Author wangdehonga
 * @Date 2020/10/26 15:33
 * @Version 1.0
 */
@Data
public class TransferQuery {

    /**
     * 币种
     */
    private String currency;

    /**
     * 划转数量
     */
    private String amount;

    /**
     * 0:母/子账户中各个账户之间划转
     * 1:母账户转子账号
     * 2:子账户转母账号 默认为0
     */
    private String type;

    /**
     * 转出账户
     * 1:币币账户
     * 3:交割合约
     * 4:法币账户
     * 5:币币杠杆账户
     * 6:资金账户
     * 9:永续合约账户
     * 12:期权合约
     * 14:挖矿账户
     * 17:借贷账户
     */
    private String from;

    /**
     * 转入账户
     * 1:币币账户
     * 3:交割合约
     * 4:法币账户
     * 5:币币杠杆账户
     * 6:资金账户
     * 9:永续合约账户
     * 12:期权合约
     * 14:挖矿账户
     * 17:借贷账户
     */
    private String to;

    /**
     * 子账号登录名，type为1、2、时，sub_account为必填项
     */
    private String sub_account;

    /**
     * 杠杆转出币对 或者USDT保证金合约转出的underlying，如：btc-usdt，仅限已开通杠杆币对或者合约的underlying。
     */
    private String instrument_id;

    /**
     * 杠杆转入币对 或者USDT保证金合约转入的underlying，如：btc-usdt，仅限已开通杠杆币对或者合约的underlying。
     */
    private String to_instrument_id;
}
