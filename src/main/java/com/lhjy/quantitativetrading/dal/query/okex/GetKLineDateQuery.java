package com.lhjy.quantitativetrading.dal.query.okex;

import lombok.Builder;
import lombok.Data;

/**
 * @description: 获取K线数据查询参数
 * @Author Dehong Wang
 * @Date 20-10-24 14:30
 * @Version 1.0
 */
@Data
@Builder
public class GetKLineDateQuery {

    /**
     * 币对
     */
    private String instrument_id;

    /**
     * 开始时间（ISO 8601标准）
     */
    private String start;

    /**
     * 结束时间（ISO 8601标准）
     */
    private String end;

    /**
     * 时间粒度，以秒为单位，默认值60 [60 180 300 900 1800 3600 7200 14400 21600 43200 86400 604800 2678400 8035200 16070400 31536000]
     *                            [1min 3min 5min 15min 30min 1hour 2hour 4hour 6hour 12hour 1day 1week 1 month 3 months 6 months 1 year]
     */
    private String granularity;

}
