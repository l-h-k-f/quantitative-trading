package com.lhjy.quantitativetrading.dal.query.okex.fund;

import lombok.Data;

/**
 * @ClassName OrdersQuery
 * @Description 下单
 * @Author wangdehonga
 * @Date 2020/10/26 17:52
 * @Version 1.0
 */
@Data
public class OrdersQuery {

    /**
     * 由您设置的订单ID来识别您的订单,格式是字母（区分大小写）+数字 或者 纯字母（区分大小写），1-32位字符 （不能重复）
     */
    private String client_oid;

    /**
     * limit或market（默认是limit）。当以market（市价）下单，order_type只能选择0（普通委托）
     */
    private String type;

    /**
     * buy 或 sell
     */
    private String side;

    /**
     * 币对名称
     */
    private String instrument_id;

    /**
     * 参数填数字
     * 0：普通委托（order type不填或填0都是普通委托）
     * 1：只做Maker（Post only）
     * 2：全部成交或立即取消（FOK）
     * 3：立即成交并取消剩余（IOC）
     */
    private String order_type;

    /**
     * 限价单特殊参数
     * 价格
     */
    private String price;

    /**
     * 限价单特殊参数
     * 买入或卖出的数量
     *
     * 市价单特殊参数
     * 卖出数量，市价卖出时必填size
     */
    private String size;

    /**
     * 市价单特殊参数
     * 买入金额，市价买入时必填notional
     */
    private String notional;
}
