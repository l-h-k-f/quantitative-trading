package com.lhjy.quantitativetrading.dal.query.binance;

import lombok.Data;

/**
 * @description:
 * @Author Dehong Wang
 * @Date 20-10-25 22:15
 * @Version 1.0
 */
@Data
public class GlobalBaseQuery {

    private String recvWindow = "5000";

    private String timestamp = Long.toString(System.currentTimeMillis());

    private String signature;
}
