package com.lhjy.quantitativetrading.dal.result.okex;

import lombok.Data;

/**
 * @description: 获取K线数据结果体
 * @Author Dehong Wang
 * @Date 20-10-24 14:34
 * @Version 1.0
 */
@Data
public class GetKLineDateResult {

    /**
     * 开始时间
     */
    private String time;

    /**
     * 开盘价格
     */
    private String open;

    /**
     * 最高价格
     */
    private String high;

    /**
     * 最低价格
     */
    private String low;

    /**
     * 收盘价格
     */
    private String close;

    /**
     * 交易量
     */
    private String volume;
}
