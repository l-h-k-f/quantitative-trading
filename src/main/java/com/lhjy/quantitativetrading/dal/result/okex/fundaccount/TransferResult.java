package com.lhjy.quantitativetrading.dal.result.okex.fundaccount;

import lombok.Data;

/**
 * @ClassName TransferResult
 * @Description
 * @Author wangdehonga
 * @Date 2020/10/26 15:37
 * @Version 1.0
 */
@Data
public class TransferResult {

    /**
     * 划转ID
     */
    private String transfer_id;

    /**
     * 划转币种
     */
    private String currency;

    /**
     * 转出账户
     */
    private String from;

    /**
     * 划转量
     */
    private String amount;

    /**
     * 	转入账户
     */
    private String to;

    /**
     * 划转结果。若是划转失败，将给出错误码提示
     */
    private String result;
}
