package com.lhjy.quantitativetrading.dal.result.okex.fundaccount;

import lombok.Data;

/**
 * @ClassName OrdersResult
 * @Description
 * @Author wangdehonga
 * @Date 2020/10/26 17:57
 * @Version 1.0
 */
@Data
public class OrdersResult {

    /**
     * 订单ID
     */
    private String order_id;

    /**
     * 由您设置的订单ID来识别您的订单
     */
    private String client_oid;

    /**
     * 下单结果。若是下单失败，将给出错误码提示
     */
    private Boolean result;

    /**
     * 错误码，下单成功时为0，下单失败时会显示相应错误码
     */
    private String error_code;

    /**
     * 	错误信息，下单成功时为空，下单失败时会显示错误信息
     */
    private String error_message;
}
