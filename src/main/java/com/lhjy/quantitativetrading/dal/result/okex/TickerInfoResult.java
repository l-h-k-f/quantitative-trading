package com.lhjy.quantitativetrading.dal.result.okex;

import lombok.Data;

/**
 * @description:
 * @Author Dehong Wang
 * @Date 20-10-24 17:40
 * @Version 1.0
 */
@Data
public class TickerInfoResult {

    /**
     * 币对名称
     */
    private String instrument_id;

    /**
     * 最新成交价
    */
    private String last;

    /**
     * 最新成交的数量
     */
    private String last_qty;

    /**
     * 卖一价
     */
    private String best_ask;

    /**
     * 卖一价对应的量
     */
    private String best_ask_size;

    /**
     * 买一价
     */
    private String best_bid;

    /**
     * 买一价对应的数量
     */
    private String best_bid_size;

    /**
     * 24小时开盘价
     */
    private String open_24h;

    /**
     * 24小时最高价
     */
    private String high_24h;

    /**
     * 24小时最低价
     */
    private String low_24h;

    /**
     * 24小时成交量，按交易货币统计
     */
    private String base_volume_24h;

    /**
     * 24小时成交量，按计价货币统计
     */
    private String quote_volume_24h;

    /**
     * 系统时间戳
     */
    private String timestamp;

}
