package com.lhjy.quantitativetrading.dal.result.okex.fundaccount;

import lombok.Data;

/**
 * @ClassName Wallet
 * @Description 资金账户信息
 * @Author wangdehonga
 * @Date 2020/10/26 14:41
 * @Version 1.0
 */
@Data
public class WalletResult {

    /**
     * 币种
     */
    private String currency;

    /**
     * 余额
     */
    private String balance;

    /**
     * 冻结（不可用）
     */
    private String hold;

    /**
     * 可用余额
     */
    private String available;
}
