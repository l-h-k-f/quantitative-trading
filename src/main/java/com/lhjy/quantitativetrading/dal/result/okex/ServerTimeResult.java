package com.lhjy.quantitativetrading.dal.result.okex;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description:
 * @Author Dehong Wang
 * @Date 20-10-21 22:28
 * @Version 1.0
 */
@Data
@ApiModel(value = "ServerTimeResult", description = "OKEX服务器时间")
public class ServerTimeResult {

    @ApiModelProperty(name = "iso", value = "UTC标准时间")
    private String iso;

    @ApiModelProperty(name = "epoch", value = "UTC时间戳")
    private String epoch;
}
