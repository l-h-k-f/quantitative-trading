package com.lhjy.quantitativetrading.dal.cenum;

/**
 * @description:
 * @Author Dehong Wang
 * @Date 20-10-23 21:50
 * @Version 1.0
 */
public enum ExchangeEnum {

    EXCHANGE_OKEX(1, "okex", "https://www.okex.com"),
    EXCHANGE_HUOBI(2, "huobi", "https://api-aws.huobi.pro"),
    EXCHANGE_BINANCE(3, "binance", "https://api.binance.com"),
    EXCHANGE_BINANCE_TEST(3, "binance", "https://testnet.binance.vision/api"),
    EXCHANGE_FTX(4, "ftx", "https://ftx.com/api"),



    ;

    private Integer code;
    private String name;
    private String host;

    ExchangeEnum(Integer code, String name, String host) {
        this.code = code;
        this.name = name;
        this.host = host;
    }

    public Integer getCode() {
        return code;
    }

    public Integer getCode(String name) {
        ExchangeEnum[] all = values();
        for (ExchangeEnum exchangeEnum : all) {
            if (name.equals(exchangeEnum.getName())) {
                return exchangeEnum.code;
            }
        }
        return null;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static String getName(Integer code) {
        ExchangeEnum[] all = values();
        for (ExchangeEnum exchangeEnum : all) {
            if (code.equals(exchangeEnum.getCode())) {
                return exchangeEnum.name;
            }
        }
        return null;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getHost() {
        return host;
    }

    public String getHost(String name) {
        ExchangeEnum[] all = values();
        for (ExchangeEnum exchangeEnum : all) {
            if (name.equals(exchangeEnum.getName())) {
                return exchangeEnum.host;
            }
        }
        return null;
    }
}
