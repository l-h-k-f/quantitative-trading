package com.lhjy.quantitativetrading.dal.dao;

import com.github.pagehelper.PageInfo;
import com.lhjy.quantitativetrading.dal.dto.base.idto.QueryCustomerIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.odto.CustomerInfoODTO;
import com.lhjy.quantitativetrading.dal.entity.TCustomerInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TCustomerInfoDAO {
    int deleteByPrimaryKey(Integer id);

    int insert(TCustomerInfo record);

    int insertSelective(TCustomerInfo record);

    TCustomerInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TCustomerInfo record);

    int updateByPrimaryKey(TCustomerInfo record);

    /**
     * 根据创建者id、客户名关键字检索
     * @param createor
     * @param customerName
     * @return
     */
    List<TCustomerInfo> selectCusList(@Param("createor") String createor, @Param("customerName") String customerName);


    /**
     * 根据 customerId、客户名关键字检索
     * @param queryCustomerIDTO 查询客户入参实体
     * @return List<CustomerInfoODTO>
     */
    List<CustomerInfoODTO> selectCusListByIdAndName(QueryCustomerIDTO queryCustomerIDTO);

    /**
     *  查询该用户（创建者）明显是否有该客户
     * @param createor 创建者
     * @param customerId 客户id
     * @return
     */
    int checkAccountForUserId(@Param("createor") String createor, @Param("customerId") String customerId);

    /**
     * 查询该用户（创建者）明显是否有该客户
     * @param createor 创建者
     * @param id api表主键
     * @return
     */
    int checkAccountForId(@Param("createor") String createor, @Param("id") Integer id);
}