package com.lhjy.quantitativetrading.dal.dao;

import com.lhjy.quantitativetrading.dal.dto.account.idto.QueryAccountIDTO;
import com.lhjy.quantitativetrading.dal.entity.TApiAccount;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TApiAccountDAO {
    int deleteByPrimaryKey(Integer id);

    int insert(TApiAccount record);

    int insertSelective(TApiAccount record);

    TApiAccount selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TApiAccount record);

    int updateByPrimaryKey(TApiAccount record);

    /**
     * 查询用于请求的账户信息
     * @param queryAccountIDTO 查询用户的账号信息
     * @return List<TApiAccount>
     */
    List<TApiAccount> selectAccount(QueryAccountIDTO queryAccountIDTO);
}