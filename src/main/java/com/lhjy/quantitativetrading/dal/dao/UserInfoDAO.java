package com.lhjy.quantitativetrading.dal.dao;

import com.lhjy.quantitativetrading.dal.dto.base.idto.QueryUserIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.idto.SignInFormIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.idto.UserEditIDTO;
import com.lhjy.quantitativetrading.dal.entity.UserInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserInfoDAO {
    int deleteByPrimaryKey(@Param("userId") String userId);

    int insert(UserInfo record);

    int insertSelective(UserInfo record);

    List<UserInfo> selectUsers(QueryUserIDTO queryUserIDTO);

    int updateSelective(UserEditIDTO userEditIDTO);

    int updateByPrimaryKey(UserInfo record);

    UserInfo selectByUserNumAndPws(SignInFormIDTO signInFormIDTO);
}