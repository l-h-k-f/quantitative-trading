package com.lhjy.quantitativetrading.dal.dto.base.idto;

import com.lhjy.quantitativetrading.utils.customannotation.Verify;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName AccountAddIDTO
 * @Description
 * @Author wangdehonga
 * @Date 2020/10/30 10:49
 * @Version 1.0
 */
@Data
@ApiModel(value = "AccountAddIDTO",description = "第三方账户信息新增入参实体")
public class AccountAddIDTO {

    @ApiModelProperty(name = "ownerId", value = "账户持有者id", required = true)
    @Verify(params = "ownerId", paramsName = "账户持有者id", required = true)
    private String ownerId;

    @ApiModelProperty(name = "exchangeId", value = "交易所编号", required = true, example = "1")
    @Verify(params = "exchangeId", paramsName = "交易所编号", required = true)
    private String exchangeId;

    @ApiModelProperty(name = "apiKey", value = "apikey", required = true)
    @Verify(params = "apiKey", paramsName = "apikey", required = true)
    private String apiKey;

    @ApiModelProperty(name = "secretKey", value = "密钥", required = true)
    @Verify(params = "secretKey", paramsName = "密钥", required = true)
    private String secretKey;

    @ApiModelProperty(name = "passPhrase", value = "okex的pass_phrase和火币的UID保留字段")
    @Verify(params = "passPhrase", paramsName = "okex的pass_phrase和火币的UID保留字段")
    private String passPhrase;

    @ApiModelProperty(name = "tag", value = "操盘标志", required = true, example = "1")
    @Verify(params = "tag", paramsName = "操盘标志", required = true)
    private Integer tag;
}
