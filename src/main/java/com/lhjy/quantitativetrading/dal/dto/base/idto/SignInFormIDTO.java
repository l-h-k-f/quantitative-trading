package com.lhjy.quantitativetrading.dal.dto.base.idto;

import com.lhjy.quantitativetrading.utils.customannotation.Verify;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName SignInFormIDTO
 * @Description 登录请求表单
 * @Author wangdehonga
 * @Date 2020/10/27 17:15
 * @Version 1.0
 */
@Data
@ApiModel(value = "SignInFormIDTO", description = "登录请求表单")
public class SignInFormIDTO {

    @ApiModelProperty(value = "用户账号", example = "11111111", required = true)
    @Verify(params = "userNum", paramsName = "用户账号", required = true)
    private String userNum;

    @ApiModelProperty(value = "用户密码", example = "123qwe", required = true)
    @Verify(params = "userPwd", paramsName = "用户密码", required = true)
    private String userPwd;
}
