package com.lhjy.quantitativetrading.dal.dto.base.odto;

import com.lhjy.quantitativetrading.dal.cenum.ExchangeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import com.lhjy.quantitativetrading.dal.constant.CommonNounsConstant;

/**
 * @ClassName AccountOwnInfoODTO
 * @Description
 * @Author wangdehonga
 * @Date 2020/10/30 14:11
 * @Version 1.0
 */
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel(value = "AccountOwnInfoODTO",description = "第三方自己账户信息查询出参实体")
public class AccountOwnInfoODTO {

    @ApiModelProperty(name = "id", value = "主键")
    private Integer id;

    @ApiModelProperty(name = "exchangeName", value = "交易所名称")
    private String exchangeName;

    @ApiModelProperty(name = "apiKey", value = "apiKey")
    private String apiKey;

    @ApiModelProperty(name = "secretKey", value = "密钥")
    private String secretKey;

    @ApiModelProperty(name = "passPhrase", value = "passPhrase")
    private String passPhrase;

    @ApiModelProperty(name = "tag", value = "操盘标志")
    private String tag;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExchangeName() {
        return exchangeName == null ? null : exchangeName.trim();
    }

    public void setExchangeName(Integer exchangeName) {
        this.exchangeName = exchangeName == null ? null : ExchangeEnum.getName(exchangeName).trim();
    }
    public String getApiKey() {
        return apiKey == null ? null : apiKey.trim();
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey == null ? null : apiKey.trim();
    }

    public String getSecretKey() {
        return secretKey == null ? null : secretKey.trim();
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey == null ? null : secretKey.trim();
    }

    public String getPassPhrase() {
        return passPhrase == null ? null : passPhrase.trim();
    }

    public void setPassPhrase(String passPhrase) {
        this.passPhrase = passPhrase == null ? null : passPhrase.trim();
    }

    public String getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        if(tag == null) {
            this.tag = null;
        } else if(tag.equals(CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK)) {
            this.tag = "实盘";
        } else if(tag.equals(CommonNounsConstant.EXCHANGEID_TAG_SIMULATED_DISK)) {
            this.tag = "模拟盘";
        }
    }
}
