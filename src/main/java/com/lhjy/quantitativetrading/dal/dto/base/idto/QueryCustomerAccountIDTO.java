package com.lhjy.quantitativetrading.dal.dto.base.idto;

import com.lhjy.quantitativetrading.dal.dto.PageInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName QueryAccountIDTO
 * @Description
 * @Author wangdehonga
 * @Date 2020/10/30 13:59
 * @Version 1.0
 */
@Data
@ApiModel(value = "QueryCustomerAccountIDTO",description = "第三方账户信息查询客户入参实体")
public class QueryCustomerAccountIDTO extends PageInfo {

    @ApiModelProperty(name = "customerName", value = "客户名")
    private String customerName;

    @ApiModelProperty(name = "exchangeId", value = "交易所编号", example = "1")
    private Integer exchangeId;

    @ApiModelProperty(name = "tag", value = "操盘标志", example = "1")
    private Integer tag;
}
