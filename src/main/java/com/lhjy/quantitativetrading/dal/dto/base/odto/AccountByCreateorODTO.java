package com.lhjy.quantitativetrading.dal.dto.base.odto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClassName AccountByCreateorODTO
 * @Description
 * @Author wangdehonga
 * @Date 2020/10/30 14:11
 * @Version 1.0
 */
@Data
@ApiModel(value = "AccountByCreateorODTO",description = "平台用户第三方账户信息查询出参实体")
public class AccountByCreateorODTO {

    @ApiModelProperty(name = "id", value = "主键")
    private Integer id;

    @ApiModelProperty(name = "userId", value = "用户id")
    private String userId;

    @ApiModelProperty(name = "userAccount", value = "用户账号")
    private String userAccount;

    @ApiModelProperty(name = "userRole", value = "角色", notes = "1：管理员（可添加用户）2：用户（仅可添加交易所账号信息）")
    private String userRole;

    @ApiModelProperty(name = "accountByOwnerODTOList", value = "客户交易所账号信息")
    private List<AccountByOwnerODTO> accountByOwnerODTOList;

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(Integer userRole) {
        if(userRole.equals(1)) {
            this.userRole = "管理员";
        } else if(userRole.equals(2)) {
            this.userRole = "用户";
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public List<AccountByOwnerODTO> getAccountByOwnerODTOList() {
        return accountByOwnerODTOList;
    }

    public void setAccountByOwnerODTOList(List<AccountByOwnerODTO> accountByOwnerODTOList) {
        this.accountByOwnerODTOList = accountByOwnerODTOList;
    }
}
