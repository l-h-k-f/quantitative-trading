package com.lhjy.quantitativetrading.dal.dto.base.idto;

import com.lhjy.quantitativetrading.utils.customannotation.Verify;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description: 增加用户信息入参
 * @Author Dehong Wang
 * @Date 20-10-29 20:50
 * @Version 1.0
 */
@Data
@ApiModel(value = "UserAddIDTO", description = "增加用户信息入参")
public class UserAddIDTO {

    @ApiModelProperty(name = "userAccount", value = "用户账号")
    @Verify(params = "userId", paramsName = "用户id", minLength = 1, maxLength = 20)
    private String userAccount;

    @ApiModelProperty(name = "userPassword", value = "用户密码")
    @Verify(params = "userId", paramsName = "用户id", minLength = 6, maxLength = 20)
    private String userPassword;
}
