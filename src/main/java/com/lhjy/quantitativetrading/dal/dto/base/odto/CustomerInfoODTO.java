package com.lhjy.quantitativetrading.dal.dto.base.odto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description:
 * @Author Dehong Wang
 * @Date 20-11-3 21:57
 * @Version 1.0
 */
@Data
@ApiModel(value = "CustomerInfoODTO",description = "客户信息出参")
public class CustomerInfoODTO {

    @ApiModelProperty(name = "id", value = "主键")
    private Integer id;

    @ApiModelProperty(name = "customerId", value = "客户id", notes = "随机生成且唯一")
    private String customerId;

    @ApiModelProperty(name = "customerName", value = "客户称谓")
    private String customerName;
}
