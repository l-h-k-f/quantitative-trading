package com.lhjy.quantitativetrading.dal.dto.account.idto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

/**
 * @description: 查询用户的账号信息
 * @Author Dehong Wang
 * @Date 20-10-23 20:34
 * @Version 1.0
 */
@Data
@Builder
@NoArgsConstructor
@ApiModel(value = "QueryAccountIDTO", description = "查询用户的账号信息")
public class QueryAccountIDTO {

    @ApiModelProperty(value = "账户持有者id", example = "12343533", required = true)
    @NotNull
    private String ownerId;

    @ApiModelProperty(value = "交易所id", example = "1", required = true)
    @NotNull
    private Integer exchangeId;

    @ApiModelProperty(value = "操盘标志", example = "1", required = true)
    private Integer tag;

}
