package com.lhjy.quantitativetrading.dal.dto.base.odto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClassName AccountByOwnerODTO
 * @Description
 * @Author wangdehonga
 * @Date 2020/10/30 14:11
 * @Version 1.0
 */
@Data
@ApiModel(value = "AccountByOwnerODTO",description = "第三方客户账户信息查询出参实体")
public class AccountByOwnerODTO {

    @ApiModelProperty(name = "id", value = "主键")
    private Integer id;

    @ApiModelProperty(name = "customerId", value = "客户id")
    private String customerId;

    @ApiModelProperty(name = "customerName", value = "客户名")
    private String customerName;

    @ApiModelProperty(name = "accountOwnInfoODTOList", value = "交易所账号信息")
    private List<AccountOwnInfoODTO> accountOwnInfoODTOList;
}
