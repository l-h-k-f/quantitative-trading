package com.lhjy.quantitativetrading.dal.dto.base.idto;

import com.lhjy.quantitativetrading.utils.customannotation.Verify;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description:
 * @Author Dehong Wang
 * @Date 20-11-3 21:44
 * @Version 1.0
 */
@Data
@ApiModel(value = "UpdateCustomerIDTO",description = "修改客户入参实体")
public class UpdateCustomerIDTO {

    @Verify(params = "id", paramsName = "主键", required = true)
    @ApiModelProperty(name = "id", value = "主键", required = true)
    private Integer id;

    @ApiModelProperty(name = "customerName", value = "客户称谓", required = true, example = "李可欣")
    @Verify(params = "customerName", paramsName = "客户称谓", required = true)
    private String customerName;
}
