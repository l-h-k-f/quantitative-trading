package com.lhjy.quantitativetrading.dal.dto.base.idto;

import com.lhjy.quantitativetrading.utils.customannotation.Verify;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName AccountEditIDTO
 * @Description
 * @Author wangdehonga
 * @Date 2020/10/30 12:45
 * @Version 1.0
 */
@Data
@ApiModel(value = "AccountEditIDTO",description = "第三方账户信息编辑入参实体")
public class AccountEditIDTO {

    @ApiModelProperty(name = "id", value = "主键", required = true, example = "1")
    @Verify(params = "id", paramsName = "主键", required = true)
    private Integer id;

    @ApiModelProperty(name = "tag", value = "操盘标志，1：实盘；2：模拟盘", example = "1")
    private Integer tag;
}
