package com.lhjy.quantitativetrading.dal.dto.base.odto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName UserInfoODTO
 * @Description 用户登录信息出参
 * @Author 王德宏
 * @Date 2020/10/28 13:38
 * @Version 1.0.0
 */
@Data
@ApiModel(value = "UserInfoODTO",description = "用户登录信息出参")
public class UserInfoODTO {

    @ApiModelProperty(name = "id", value = "主键")
    private Integer id;

    @ApiModelProperty(name = "userId", value = "用户id", notes = "随机生成且唯一")
    private String userId;

    @ApiModelProperty(name = "userAccount", value = "用户账号")
    private String userAccount;

    @ApiModelProperty(name = "userPassword", value = "用户密码")
    private String userPassword;

    @ApiModelProperty(name = "userRole", value = "角色", notes = "1：管理员（可添加用户）2：用户（仅可添加交易所账号信息）")
    private Integer userRole;

    @ApiModelProperty(name = "token")
    private String token;
}