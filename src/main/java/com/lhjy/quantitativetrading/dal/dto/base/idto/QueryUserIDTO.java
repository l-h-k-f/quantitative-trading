package com.lhjy.quantitativetrading.dal.dto.base.idto;

import com.lhjy.quantitativetrading.dal.dto.PageInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description: 用户信息查询入参实体
 * @Author Dehong Wang
 * @Date 20-10-29 21:15
 * @Version 1.0
 */
@Data
@ApiModel(value = "QueryUserIDTO",description = "用户信息查询入参实体")
public class QueryUserIDTO extends PageInfo {

    @ApiModelProperty(name = "userId", value = "用户id", notes = "随机生成且唯一")
    private String userId;

    @ApiModelProperty(name = "userAccount", value = "用户账号")
    private String userAccount;

    @ApiModelProperty(name = "userRole", value = "角色", notes = "1：管理员（可添加用户）2：用户（仅可添加交易所账号信息）")
    private Integer userRole;
}
