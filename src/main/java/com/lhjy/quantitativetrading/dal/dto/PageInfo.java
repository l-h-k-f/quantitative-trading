package com.lhjy.quantitativetrading.dal.dto;

import com.lhjy.quantitativetrading.utils.customannotation.Verify;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

/**
 * @description: 分页信息
 * @Author Dehong Wang
 * @Date 20-10-29 21:15
 * @Version 1.0
 */
@Data
@ApiModel(value = "PageInfo", description = "分页信息")
public class PageInfo {

    @ApiModelProperty(value = "页码", example = "1", required = true)
    @Verify(params = "pageNum", paramsName = "页码", required = true)
    private Integer pageNum;

    @ApiModelProperty(value = "分页大小", example = "10", required = true)
    @Verify(params = "pageSize", paramsName = "分页大小", required = true)
    private Integer pageSize;
}
