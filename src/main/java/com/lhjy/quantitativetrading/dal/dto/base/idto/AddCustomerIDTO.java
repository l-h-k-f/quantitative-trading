package com.lhjy.quantitativetrading.dal.dto.base.idto;

import com.lhjy.quantitativetrading.utils.customannotation.Verify;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description:
 * @Author Dehong Wang
 * @Date 20-11-3 21:44
 * @Version 1.0
 */
@Data
@ApiModel(value = "AddCustomerIDTO",description = "新增客户入参实体")
public class AddCustomerIDTO {

    @ApiModelProperty(name = "customerName", value = "客户称谓", required = true, example = "李可欣")
    @Verify(params = "customerName", paramsName = "客户称谓", required = true)
    private String customerName;
}
