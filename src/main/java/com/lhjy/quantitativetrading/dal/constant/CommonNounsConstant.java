package com.lhjy.quantitativetrading.dal.constant;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName CommonNounsConstant
 * @Description 常用名词常量池
 * @Author wangdehonga
 * @Date 2020/10/21 19:58
 * @Version 1.0
 */
public class CommonNounsConstant {

    public static final String HMAC_SHA256 = "HmacSHA256";
    public static final String MD5 = "MD5";
    public static final Integer SEND_REQUEST_TIME_OUT = 10000;
    public static final Integer READ_TIME_OUT = 10000;
    public static final String REQUEST_CONTENT_TYPE = "application/json";
    public static final List<Integer> resultStatusArray = Arrays.asList(
            400,401,429,500);

    public static final String UTF_8_NAME = "UTF-8";

    /**
     * session 存储用户信息 key
     */
    public static final String SESSION_SAVE_USERINFO_NAME = "UserInfo";
    /**
     * session 存储token信息 key
     */
    public static final String SESSION_SAVE_TOKEN_NAME = "token";

    /**
     * 操盘手操作盘符为实盘操作
     */
    public static final Integer EXCHANGEID_TAG_REAL_DISK = 1;
    /**
     * 操盘手操作盘符为模拟盘操作
     */
    public static final Integer EXCHANGEID_TAG_SIMULATED_DISK= 2;

    /**
     * 用户平台标识
     */
    public static final String USER_TAB= "Du";

    /**
     * 客户平台标识
     */
    public static final String CUS_TAB= "Db";

}
