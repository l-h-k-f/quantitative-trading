package com.lhjy.quantitativetrading.dal.constant;

/**
 * @description:
 * @Author Dehong Wang
 * @Date 20-10-21 23:27
 * @Version 1.0
 */
public class RequestHeadersConstant {

    /**
     * okex
     */
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String ACCESS_KEY = "OK-ACCESS-KEY";
    public static final String ACCESS_SIGN = "OK-ACCESS-SIGN";
    public static final String ACCESS_PASSPHRASE = "OK-ACCESS-PASSPHRASE";
    public static final String ACCESS_TIMESTAMP = "OK-ACCESS-TIMESTAMP";

    /**
     * huobi
     */
    public static final String ACCESS_KEY_ID = "AccessKeyId";
    public static final String SIGNATURE_METHOD = "SignatureMethod";
    public static final String SIGNATURE_METHOD_VALUE = "HmacSHA256";
    public static final String SIGNATURE_VERSION = "SignatureVersion";
    public static final String SIGNATURE_VERSION_VALUE = "2";
    public static final String TIMESTAMP = "Timestamp";
    public static final String SIGNATURE = "Signature";

    /**
     * bian
     */
    public static final String BIAN_APP_KAY = "X-MBX-APIKEY";

    /**
     * FTX
     */
    public static final String FTX_KEY = "FTX-KEY";
    public static final String FTX_TS = "FTX-TS";
    public static final String FTX_SIGN = "FTX-SIGN";
    //可选 要使用的子帐户的URI编码名称
    public static final String FTX_SUBACCOUNT = "FTX-SUBACCOUNT";
}
