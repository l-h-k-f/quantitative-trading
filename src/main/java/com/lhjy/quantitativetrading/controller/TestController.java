package com.lhjy.quantitativetrading.controller;

import com.lhjy.quantitativetrading.api.okex.CoinsApi;
import com.lhjy.quantitativetrading.api.okex.FundAccountApi;
import com.lhjy.quantitativetrading.dal.constant.CommonNounsConstant;
import com.lhjy.quantitativetrading.dal.entity.UserInfo;
import com.lhjy.quantitativetrading.dal.query.okex.fund.TransferQuery;
import com.lhjy.quantitativetrading.dal.result.okex.fundaccount.TransferResult;
import com.lhjy.quantitativetrading.utils.http.RequestUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName TestController
 * @Description TODO
 * @Author wangdehonga
 * @Date 2020/10/20 10:33
 * @Version 1.0
 */
@Api(value = "量化交易--测试")
@RestController
@RequestMapping("/test")
public class TestController {


    @Autowired
    CoinsApi coinsApi;
    @Autowired
    RequestUtils requestUtils;
    @Autowired
    FundAccountApi fundAccountApi;

    @ApiOperation(value = "测试链接交易所服务器")
    @GetMapping("/testOkex")
    public TransferResult testOkex(HttpServletRequest request, String time ) throws Exception {

        UserInfo userInfo = new UserInfo();
        userInfo.setId(1);
        userInfo.setUserId("88888888");
        userInfo.setUserAccount("Adnis");
        userInfo.setUserPassword("666666");
        //30分钟有效时间
        request.getSession().setMaxInactiveInterval(30*60);
        request.getSession().setAttribute(CommonNounsConstant.SESSION_SAVE_USERINFO_NAME, userInfo);

        TransferQuery transferQuery = new TransferQuery();
        transferQuery.setCurrency("MNUSDT");
        transferQuery.setAmount("0.005");
        transferQuery.setType("0");
        transferQuery.setFrom("1");
        transferQuery.setTo("3");

        return  fundAccountApi.Transfer(transferQuery,"88888888");


//        GetKLineDateQuery getKLineDateQuery = GetKLineDateQuery.builder().instrument_id("MNBTC-MNUSDT").start("2020-01-24T00:00:00.000Z").end(DateUtils.getUtcDateForOkex()).granularity(time).build();
//        return coinsApi.getKLineDate(getKLineDateQuery, "888888888");
//return "";
//        System.out.println(time);
//
//        //获取Okex服务器时间;
        //return  coinsApi.getTickerInfoOf24h("MNBTC-MNUSDT","88888888");
//        GetKLineDateQuery getKLineDateQuery = GetKLineDateQuery.builder().instrument_id("MNBTC-MNUSDT").start("2020-10-24T00:00:00.000Z").end("2020-10-25T00:00:00.000Z").granularity(time).build();
//        return  fundAccountApi.getWallet(getKLineDateQuery);

//        return accountDAO.selectByPrimaryKey(1);
        //huobi
//        Map<String, String> sign = HuobiSignatureUtils.signAndUrl(IdentityRelatedInformation.HUOBI_ACCESS_KEY, IdentityRelatedInformation.HUOBI_SECRET_KEY,
//                "get", "/v2/market-status", null);
//        return HttpUtils.execute(requestUtils.getApiAndSignBianHasHeaderRequestForGet(sign.get("url")));

        //bian--get
//        return HttpUtils.execute(requestUtils.getApiAndSignBianHasHeaderRequestForGet("/sapi/v1/capital/config/getall", "88888888", new GlobalBaseQuery()));

        //FTX
//        String time = Long.toString(System.currentTimeMillis());
//        return HttpUtils.execute(requestUtils.getFtxRequestForGet("/account", null, "88888888", null));

    }
}
