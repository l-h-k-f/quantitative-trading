package com.lhjy.quantitativetrading.controller.base;

import com.lhjy.quantitativetrading.dal.dto.base.idto.SignInFormIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.odto.UserInfoODTO;
import com.lhjy.quantitativetrading.service.base.UserOperationService;
import com.lhjy.quantitativetrading.utils.common.MyException;
import com.lhjy.quantitativetrading.utils.common.RedisUtil;
import com.lhjy.quantitativetrading.utils.common.ReturnMsg;
import com.lhjy.quantitativetrading.utils.customannotation.NoAuthentication;
import com.lhjy.quantitativetrading.utils.customannotation.Verifys;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

/**
 * @ClassName SignController
 * @Description
 * @Author wangdehonga
 * @Date 2020/10/27 17:12
 * @Version 1.0
 */
@Api(tags = "用户--登录")
@RestController
@RequestMapping("/base/sign")
public class SignController {
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private UserOperationService userOperationService;

    @Verifys
    @NoAuthentication
    @ApiOperation(value = "用户登录")
    @PostMapping("/sign_in")
    public UserInfoODTO SignIn(@RequestBody SignInFormIDTO signInFormIDTO, HttpServletRequest request) {
        UserInfoODTO userInfoODTO = userOperationService.checkUserInfoforSignIn(signInFormIDTO);
        Set<String> keys = redisUtil.getKeysList(userInfoODTO.getToken());
        if (!CollectionUtils.isEmpty(keys)) {
            redisUtil.del(keys);
        }
        //存储用户信息
        if (!redisUtil.set(userInfoODTO.getToken(), userInfoODTO, 1800)) {
            throw new MyException(ReturnMsg.USER_INFORMATION_STORAGE_CACHE_FAILED);
        }
        return userInfoODTO;
    }

    @ApiOperation(value = "用户退出")
    @GetMapping("/sign_out")
    public void SignOut(HttpServletRequest request) {
        Set<String> keys = redisUtil.getKeysList(request.getHeader("token"));
        if (!CollectionUtils.isEmpty(keys)) {
            redisUtil.del(keys);
        }
    }
}
