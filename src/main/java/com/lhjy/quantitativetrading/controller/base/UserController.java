package com.lhjy.quantitativetrading.controller.base;

import com.github.pagehelper.PageInfo;
import com.lhjy.quantitativetrading.dal.dto.base.idto.QueryUserIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.idto.UserAddIDTO;
import com.lhjy.quantitativetrading.dal.dto.base.idto.UserEditIDTO;
import com.lhjy.quantitativetrading.dal.entity.UserInfo;
import com.lhjy.quantitativetrading.service.base.UserOperationService;
import com.lhjy.quantitativetrading.utils.customannotation.Verifys;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @description:
 * @Author Dehong Wang
 * @Date 20-10-29 20:38
 * @Version 1.0
 */
@Api(tags = "用户--用户信息维护")
@RestController
@RequestMapping("/base/info")
public class UserController {

    @Autowired
    private UserOperationService userOperationService;

    @Verifys
    @ApiOperation(value = "添加用户")
    @PostMapping("/addUser")
    public void addUser(@RequestBody UserAddIDTO userAddIDTO) {
        //userOperationService.insertUserInfo(userAddIDTO);
    }

    @Verifys
    @ApiOperation(value = "修改用户信息")
    @PostMapping("/editUser")
    public void editUser(@RequestBody UserEditIDTO userEditIDTO) {
        //userOperationService.updateUserInfo(userEditIDTO);
    }

    @ApiOperation(value = "删除用户信息")
    @GetMapping("/removeUser")
    @ApiImplicitParam(name = "userId", value = "用户id", required = true, example = "88888888")
    public void removeUser(@RequestParam String userId) {
        //userOperationService.deleteUser(userId);
    }

    @Verifys
    @ApiOperation(value = "查询用户信息")
    @PostMapping("/queryUsers")
    public PageInfo<UserInfo> queryUsers(@RequestBody QueryUserIDTO queryUserIDTO) {
        //return userOperationService.selectUsers(queryUserIDTO);
        return null;
    }
}
