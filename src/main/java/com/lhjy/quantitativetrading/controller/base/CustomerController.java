package com.lhjy.quantitativetrading.controller.base;

import com.github.pagehelper.PageInfo;
import com.lhjy.quantitativetrading.dal.dto.base.idto.*;
import com.lhjy.quantitativetrading.dal.dto.base.odto.CustomerInfoODTO;
import com.lhjy.quantitativetrading.dal.entity.UserInfo;
import com.lhjy.quantitativetrading.service.base.CustomerService;
import com.lhjy.quantitativetrading.service.base.UserOperationService;
import com.lhjy.quantitativetrading.utils.customannotation.Verifys;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @description:
 * @Author Dehong Wang
 * @Date 20-10-29 20:38
 * @Version 1.0
 */
@Api(tags = "用户--客户信息维护")
@RestController
@RequestMapping("/base/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Verifys
    @ApiOperation(value = "添加客户")
    @PostMapping("/addCus")
    public void addCus(@RequestBody AddCustomerIDTO addCustomerIDTO) {
        customerService.insertCustomer(addCustomerIDTO);
    }

    @Verifys
    @ApiOperation(value = "修改客户信息")
    @PostMapping("/editCus")
    public void editCus(@RequestBody UpdateCustomerIDTO updateCustomerIDTO) {
        customerService.updateCustomer(updateCustomerIDTO);
    }

    @ApiOperation(value = "删除客户信息")
    @GetMapping("/removeCus")
    @ApiImplicitParam(name = "id", value = "主键", required = true, example = "1")
    public void removeCus(@RequestParam Integer id) {
        customerService.deleteCus(id);
    }

    @Verifys
    @ApiOperation(value = "查询客户信息")
    @PostMapping("/queryCuss")
    public PageInfo<CustomerInfoODTO> queryCuss(@RequestBody QueryCustomerIDTO queryCustomerIDTO) {
        return customerService.queryCusInfo(queryCustomerIDTO);
    }
}
