package com.lhjy.quantitativetrading.controller.base;

import com.github.pagehelper.PageInfo;
import com.lhjy.quantitativetrading.dal.dto.base.idto.*;
import com.lhjy.quantitativetrading.dal.dto.base.odto.AccountByCreateorODTO;
import com.lhjy.quantitativetrading.dal.dto.base.odto.AccountByOwnerODTO;
import com.lhjy.quantitativetrading.dal.dto.base.odto.AccountOwnInfoODTO;
import com.lhjy.quantitativetrading.service.base.ExchangeService;
import com.lhjy.quantitativetrading.utils.customannotation.Verifys;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName AccountController
 * @Description
 * @Author wangdehonga
 * @Date 2020/10/30 10:37
 * @Version 1.0
 */
@Api(tags = "用户--第三方账户信息维护")
@RestController
@RequestMapping("/base/account")
public class AccountController {

    @Autowired
    private ExchangeService exchangeService;



    @Verifys
    @ApiOperation(value = "添加账户")
    @PostMapping("/addAccount")
    public void addAccount(@RequestBody AccountAddIDTO accountAddIDTO) {
        exchangeService.insertAccount(accountAddIDTO);
    }

    @Verifys
    @ApiOperation(value = "修改账户")
    @PostMapping("/editAccount")
    public void editAccount(@RequestBody AccountEditIDTO accountEditIDTO) {
        exchangeService.updateAccount(accountEditIDTO);
    }

    @ApiOperation(value = "删除账户")
    @GetMapping("/removeAccount")
    @ApiImplicitParam(name = "id", value = "主键", required = true, example = "1")
    public void removeAccount(@RequestParam Integer id) {
        exchangeService.deleteAccount(id);
    }

    @Verifys
    @ApiOperation(value = "查询本人账户")
    @PostMapping("/queryOwnAccount")
    public PageInfo<AccountOwnInfoODTO> queryOwnAccount(@RequestBody QueryOwnAccountIDTO queryOwnAccountIDTO) {
        return exchangeService.selectOwnAccounts(queryOwnAccountIDTO);
    }

    @ApiOperation(value = "查询客户账户")
    @PostMapping("/queryCustomerAccount")
    public List<AccountByOwnerODTO> queryCustomerAccount(@RequestBody QueryCustomerAccountIDTO queryCustomerAccountIDTO) {
        return exchangeService.selectCustomerAccounts(queryCustomerAccountIDTO);
    }

    @ApiOperation(value = "查询用户（全部）账户--需管理员权限")
    @PostMapping("/queryAllAccount")
    public List<AccountByCreateorODTO> queryAllAccount(@RequestBody QueryUserAccountIDTO queryUserAccountIDTO) {
        return exchangeService.selectCustomerAccountsForUser(queryUserAccountIDTO);
    }
}
