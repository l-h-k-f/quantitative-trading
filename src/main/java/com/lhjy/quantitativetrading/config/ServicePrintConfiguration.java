package com.lhjy.quantitativetrading.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;

/**
 * @description:
 * @Author Dehong Wang
 * @Date 20-10-24 0:24
 * @Version 1.0
 */
@Slf4j
@Configuration
@ConditionalOnWebApplication
public class ServicePrintConfiguration  implements CommandLineRunner {

    @Value("${spring.application.name}")
    private String projectName;
    @Value("${server.port}")
    private String port;
    @Value("${server.servlet.context-path}")
    private String contextPath;

    private String url;
    @Value("${spring.profiles.active}")
    private String active;

    @Override
    public void run(String... args) throws Exception {
        url = InetAddress.getLocalHost().getHostAddress();
        System.out.println("----------------------------------------------------------");
        System.out.println("\tApplication '"+projectName+"' is running! Access URLs:");
        System.out.println("\tLocal: \t\thttp://localhost:"+port+contextPath);
        System.out.println("\tExternal: \thttp://"+url+":"+port+contextPath);
        System.out.println("\tSwagger: \thttp://"+url+":"+port+contextPath+"/doc.html");
        System.out.println("\tProfile(s): \t\t["+active+"]");
        System.out.println("----------------------------------------------------------");
    }
}
