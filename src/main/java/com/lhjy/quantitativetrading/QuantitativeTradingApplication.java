package com.lhjy.quantitativetrading;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author wangdehonga
 */
@SpringBootApplication
@ComponentScan(basePackages = "com")
@MapperScan("com.lhjy.quantitativetrading.dal.dao")
public class QuantitativeTradingApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuantitativeTradingApplication.class, args);
    }

}
