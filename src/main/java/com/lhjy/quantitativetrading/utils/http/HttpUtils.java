package com.lhjy.quantitativetrading.utils.http;

import com.alibaba.fastjson.JSON;
import com.lhjy.quantitativetrading.dal.constant.CommonNounsConstant;
import com.lhjy.quantitativetrading.utils.common.APIException;
import com.lhjy.quantitativetrading.utils.common.HttpResult;
import com.lhjy.quantitativetrading.utils.common.MyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;


/**
 * @description:
 * @Author Dehong Wang
 * @Date 20-10-21 22:40
 * @Version 1.0
 */
@Slf4j
@Component
public class HttpUtils {
    private static ConnectionPool connectionPool =
            new ConnectionPool(20, 300, TimeUnit.SECONDS);
    private static Boolean LATENCY_DEBUG_SWATCH = Boolean.FALSE;

    private static LinkedBlockingQueue<NetworkLatency> LATENCY_DEBUG_QUEUE = new LinkedBlockingQueue<>();
    private static final OkHttpClient client = new OkHttpClient.Builder()
            .followSslRedirects(false)
            .followRedirects(false)
            .connectTimeout(5000, TimeUnit.MILLISECONDS)
            .readTimeout(5000, TimeUnit.MILLISECONDS)
            .writeTimeout(5000, TimeUnit.MILLISECONDS)
            .connectionPool(connectionPool)
            .addNetworkInterceptor(new Interceptor() {
                @NotNull
                @Override
                public Response intercept(@NotNull Chain chain) throws IOException {
                    Request request = chain.request();

                    Long startNano = System.nanoTime();

                    Response response = chain.proceed(request);

                    Long endNano = System.nanoTime();

                    if (LATENCY_DEBUG_SWATCH) {
                        LATENCY_DEBUG_QUEUE.add(new NetworkLatency(request.url().url().getPath(), startNano, endNano));
                    }

                    return response;
                }
            })
            .build();

    public static String execute(Request request) {
        Response response = null;
        String str = null;
        try {
            log.debug("[Request URL]{}", request.url());
            response = client.newCall(request).execute();
            //获取状态码
            final int status = response.code();
            //获取错误信息
            final String message = new StringBuilder().append(response.code()).append(" / ").append(response.message()).toString();
            if (response.isSuccessful()) {
                str = response.body().string();
                response.close();
            } else if (CommonNounsConstant.resultStatusArray.contains(status)) {
                final HttpResult result = JSON.parseObject(new String(response.body().bytes()), HttpResult.class);
                if(result.getCode() == 0 && result.getMessage() == null){
                    throw new APIException(result.getErrorCode(),result.getErrorMessage());
                }else{
                    throw new APIException(result.getCode(), result.getMessage());
                }
            } else {
                throw new APIException(message);
            }
            log.debug("[Response]{}", str);
            return str;
        } catch (final IOException e) {
            throw new APIException("APIClient executeSync exception.", e);
        }
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class NetworkLatency {

        private String path;

        private Long startNanoTime;

        private Long endNanoTime;
    }
}
