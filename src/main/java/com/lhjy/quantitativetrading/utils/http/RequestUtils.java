package com.lhjy.quantitativetrading.utils.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lhjy.quantitativetrading.dal.cenum.ExchangeEnum;
import com.lhjy.quantitativetrading.dal.constant.CommonNounsConstant;
import com.lhjy.quantitativetrading.dal.constant.RequestHeadersConstant;
import com.lhjy.quantitativetrading.dal.dao.TApiAccountDAO;
import com.lhjy.quantitativetrading.dal.dto.account.idto.QueryAccountIDTO;
import com.lhjy.quantitativetrading.dal.entity.TApiAccount;
import com.lhjy.quantitativetrading.utils.common.FormatUtils;
import com.lhjy.quantitativetrading.utils.common.MyException;
import com.lhjy.quantitativetrading.utils.common.ReturnMsg;
import com.lhjy.quantitativetrading.utils.date.DateUtils;
import com.lhjy.quantitativetrading.utils.signature.BinanceSignatureUtils;
import com.lhjy.quantitativetrading.utils.signature.FtxSignatureUtils;
import com.lhjy.quantitativetrading.utils.signature.HuobiSignatureUtils;
import com.lhjy.quantitativetrading.utils.signature.OkexSignatureUtils;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Optional;

/**
 * @description: 获取请求
 * @Author Dehong Wang
 * @Date 20-10-22 0:55
 * @Version 1.0
 */
@Component
public class RequestUtils {

    @Autowired
    private TApiAccountDAO tApiAccountDAO;

    /**
     * okex实盘操作GET请求体获取
     * @param uri 接口路径
     * @param queryParam 请求入参，必须使用实体类传参，且属性名必须与接口文档中一致
     * @param ownerId 账户持有者id
     * @return
     */
    public Request getOkexRequestForGet(String uri, Object queryParam, String ownerId) {
        String time = DateUtils.getUtcDateForOkex();
        TApiAccount TApiAccount = Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_OKEX.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
        try {
            if(!ObjectUtils.isEmpty(queryParam)) {
                return new Request.Builder()
                        .url(ExchangeEnum.EXCHANGE_OKEX.getHost()+uri+"?"+FormatUtils.formatQueryParamsForGet(queryParam))
                        .addHeader(RequestHeadersConstant.CONTENT_TYPE, CommonNounsConstant.REQUEST_CONTENT_TYPE)
                        .addHeader(RequestHeadersConstant.ACCESS_KEY, Optional.ofNullable(TApiAccount.getApiKey()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_API_KEY_IS_EMPTY)))
                        .addHeader(RequestHeadersConstant.ACCESS_SIGN, OkexSignatureUtils.sign(time, "GET", uri,
                                FormatUtils.formatQueryParamsForGet(queryParam), null, Optional.ofNullable(TApiAccount.getSecretKey()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_SECRET_KEY_IS_EMPTY))))
                        .addHeader(RequestHeadersConstant.ACCESS_PASSPHRASE, Optional.ofNullable(TApiAccount.getPassPhrase()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_CULUNM_IS_EMPTY)))
                        .addHeader(RequestHeadersConstant.ACCESS_TIMESTAMP, time)
                        .get()
                        .build();
            }
            return new Request.Builder()
                    .url(ExchangeEnum.EXCHANGE_OKEX.getHost()+uri)
                    .addHeader(RequestHeadersConstant.CONTENT_TYPE, CommonNounsConstant.REQUEST_CONTENT_TYPE)
                    .addHeader(RequestHeadersConstant.ACCESS_KEY, Optional.ofNullable(TApiAccount.getApiKey()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_API_KEY_IS_EMPTY)))
                    .addHeader(RequestHeadersConstant.ACCESS_SIGN, OkexSignatureUtils.sign(time, "GET", uri,
                            null, null, Optional.ofNullable(TApiAccount.getSecretKey()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_SECRET_KEY_IS_EMPTY))))
                    .addHeader(RequestHeadersConstant.ACCESS_PASSPHRASE, Optional.ofNullable(TApiAccount.getPassPhrase()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_CULUNM_IS_EMPTY)))
                    .addHeader(RequestHeadersConstant.ACCESS_TIMESTAMP, time)
                    .get()
                    .build();
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
    }

    /**
     * okex模拟盘操作GET请求体获取
     * @param uri 接口路径
     * @param queryParam 请求入参，必须使用实体类传参，且属性名必须与接口文档中一致
     * @param ownerId 账户持有者id
     * @return
     */
    public Request getOkexRequestForGetMN(String uri, Object queryParam, String ownerId) {
        String time = DateUtils.getUtcDateForOkex();
        TApiAccount TApiAccount = Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_OKEX.getCode(), CommonNounsConstant.EXCHANGEID_TAG_SIMULATED_DISK)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
        try {
            if(!ObjectUtils.isEmpty(queryParam)) {
                return new Request.Builder()
                        .url(ExchangeEnum.EXCHANGE_OKEX.getHost()+uri+"?"+FormatUtils.formatQueryParamsForGet(queryParam))
                        .addHeader(RequestHeadersConstant.CONTENT_TYPE, CommonNounsConstant.REQUEST_CONTENT_TYPE)
                        .addHeader(RequestHeadersConstant.ACCESS_KEY, Optional.ofNullable(TApiAccount.getApiKey()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_API_KEY_IS_EMPTY)))
                        .addHeader(RequestHeadersConstant.ACCESS_SIGN, OkexSignatureUtils.sign(time, "GET", uri,
                                FormatUtils.formatQueryParamsForGet(queryParam), null, Optional.ofNullable(TApiAccount.getSecretKey()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_SECRET_KEY_IS_EMPTY))))
                        .addHeader(RequestHeadersConstant.ACCESS_PASSPHRASE, Optional.ofNullable(TApiAccount.getPassPhrase()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_CULUNM_IS_EMPTY)))
                        .addHeader(RequestHeadersConstant.ACCESS_TIMESTAMP, time)
                        .addHeader("x-simulated-trading", "1")
                        .get()
                        .build();
            }
            return new Request.Builder()
                    .url(ExchangeEnum.EXCHANGE_OKEX.getHost()+uri)
                    .addHeader(RequestHeadersConstant.CONTENT_TYPE, CommonNounsConstant.REQUEST_CONTENT_TYPE)
                    .addHeader(RequestHeadersConstant.ACCESS_KEY, Optional.ofNullable(TApiAccount.getApiKey()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_API_KEY_IS_EMPTY)))
                    .addHeader(RequestHeadersConstant.ACCESS_SIGN, OkexSignatureUtils.sign(time, "GET", uri,
                            null, null, Optional.ofNullable(TApiAccount.getSecretKey()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_SECRET_KEY_IS_EMPTY))))
                    .addHeader(RequestHeadersConstant.ACCESS_PASSPHRASE, Optional.ofNullable(TApiAccount.getPassPhrase()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_CULUNM_IS_EMPTY)))
                    .addHeader(RequestHeadersConstant.ACCESS_TIMESTAMP, time)
                    .addHeader("x-simulated-trading", "1")
                    .get()
                    .build();
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
    }

    /**
     * okex实盘操作POST请求体获取
     * @param uri 接口路径
     * @param body 请求入参，必须使用实体类传参，且属性名必须与接口文档中一致
     * @param ownerId 账户持有者id
     * @return
     */
    public Request getOkexRequestForPost(String uri, Object body, String ownerId) {
        String time = DateUtils.getUtcDateForOkex();
        TApiAccount TApiAccount = Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_OKEX.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
        try {
            return new Request.Builder()
                    .url(ExchangeEnum.EXCHANGE_OKEX.getHost() + uri)
                    .addHeader(RequestHeadersConstant.CONTENT_TYPE, CommonNounsConstant.REQUEST_CONTENT_TYPE)
                    .addHeader(RequestHeadersConstant.ACCESS_KEY, Optional.ofNullable(TApiAccount.getApiKey()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_API_KEY_IS_EMPTY)))
                    .addHeader(RequestHeadersConstant.ACCESS_SIGN, OkexSignatureUtils.sign(time, "POST", uri,
                            null, JSONObject.toJSONString(body), Optional.ofNullable(TApiAccount.getSecretKey()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_SECRET_KEY_IS_EMPTY))))
                    .addHeader(RequestHeadersConstant.ACCESS_PASSPHRASE, Optional.ofNullable(TApiAccount.getPassPhrase()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_CULUNM_IS_EMPTY)))
                    .addHeader(RequestHeadersConstant.ACCESS_TIMESTAMP, time)
                    .post(RequestBody.create(MediaType.parse(CommonNounsConstant.REQUEST_CONTENT_TYPE), JSONObject.toJSONString(body)))
                    .build();
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
    }

    /**
     * okex模拟盘操作POST请求体获取
     * @param uri 接口路径
     * @param body Post请求入参，必须使用实体类传参，且属性名必须与接口文档中一致
     * @param ownerId 账户持有者id
     * @return
     */
    public Request getOkexRequestForPostMN(String uri, Object body, String ownerId) {
        String time = DateUtils.getUtcDateForOkex();
        TApiAccount TApiAccount = Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_OKEX.getCode(), CommonNounsConstant.EXCHANGEID_TAG_SIMULATED_DISK)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
        try {
            return new Request.Builder()
                    .url(ExchangeEnum.EXCHANGE_OKEX.getHost() + uri)
                    .addHeader(RequestHeadersConstant.CONTENT_TYPE, CommonNounsConstant.REQUEST_CONTENT_TYPE)
                    .addHeader(RequestHeadersConstant.ACCESS_KEY, Optional.ofNullable(TApiAccount.getApiKey()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_API_KEY_IS_EMPTY)))
                    .addHeader(RequestHeadersConstant.ACCESS_SIGN, OkexSignatureUtils.sign(time, "POST", uri,
                            null, JSONObject.toJSONString(body), Optional.ofNullable(TApiAccount.getSecretKey()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_SECRET_KEY_IS_EMPTY))))
                    .addHeader(RequestHeadersConstant.ACCESS_PASSPHRASE, Optional.ofNullable(TApiAccount.getPassPhrase()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_CULUNM_IS_EMPTY)))
                    .addHeader(RequestHeadersConstant.ACCESS_TIMESTAMP, time)
                    .addHeader("x-simulated-trading", "1")
                    .post(RequestBody.create(MediaType.parse(CommonNounsConstant.REQUEST_CONTENT_TYPE), JSONObject.toJSONString(body)))
                    .build();
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
    }


    /**
     * huobi NONE GET 请求体封装
     * @param uri 接口路径
     * @param queryObject get请求入参，必须使用实体类传参，且属性名必须与接口文档中一致
     * @return
     */
    public Request getRequestHuoBiForGet(String uri, Object queryObject) {
        return new Request.Builder()
                .url(ExchangeEnum.EXCHANGE_HUOBI.getHost() + uri +"?"+FormatUtils.formatQueryParamsForGet(queryObject))
                .get()
                .build();
    }

    /**
     * huobi NONE POST 请求体封装
     * @param uri 接口路径
     * @param body 请求入参，必须使用实体类传参，且属性名必须与接口文档中一致
     * @return
     */
    public Request getRequestHuoBiForPost(String uri, Object body) {
        return new Request.Builder()
                .url(ExchangeEnum.EXCHANGE_HUOBI.getHost() + uri)
                .post(RequestBody.create(MediaType.parse(CommonNounsConstant.REQUEST_CONTENT_TYPE), JSONObject.toJSONString(body)))
                .build();
    }

    /**
     * huobi SIGN GET 请求体封装
     * @param uri 接口路径
     * @param queryObject get请求入参
     * @return
     */
    public Request getSignHuoBiRequestForGet(String uri, Map<String, String> queryObject, String ownerId) {
        String time = DateUtils.getUtcDateForHuoBi();
        TApiAccount TApiAccount = Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_HUOBI.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
        try {
            return new Request.Builder()
                    .url(HuobiSignatureUtils.signAndUrl(time,
                            TApiAccount.getApiKey(), TApiAccount.getSecretKey(), "GET", uri, queryObject))
                    .get()
                    .build();
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
    }

    /**
     * huobi SIGN POST 请求体封装
     * @param uri 接口路径
     * @param body 请求入参，必须使用实体类传参，且属性名必须与接口文档中一致
     * @return
     */
    public Request getSignHuoBiRequestForPost(String uri, Object body, String ownerId) {
        String time = DateUtils.getUtcDateForHuoBi();
        TApiAccount TApiAccount = Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_HUOBI.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
        try {
            return new Request.Builder()
                    .url(HuobiSignatureUtils.signAndUrl(time,
                            TApiAccount.getApiKey(), TApiAccount.getSecretKey(), "POST", uri, null))
                    .post(RequestBody.create(MediaType.parse(CommonNounsConstant.REQUEST_CONTENT_TYPE), JSONObject.toJSONString(body)))
                    .build();
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
    }


    /**
     * Bian NONE GET 请求体封装
     * @param uri 接口路径
     * @param queryObject get请求入参，必须使用实体类传参，且属性名必须与接口文档中一致
     * @return
     */
    public Request getRequestBianForGet(String uri, Object queryObject) {
        return new Request.Builder()
                .url(ExchangeEnum.EXCHANGE_BINANCE.getHost() + uri +"?"+FormatUtils.formatQueryParamsForGet(queryObject))
                .get()
                .build();
    }

    /**
     * Bian NONE POST 请求体封装
     * @param uri 接口路径
     * @param body 请求入参，必须使用实体类传参，且属性名必须与接口文档中一致
     * @return
     */
    public Request getRequestBianForPost(String uri, Object body) {
        return new Request.Builder()
                .url(ExchangeEnum.EXCHANGE_BINANCE.getHost() + uri)
                .post(RequestBody.create(MediaType.parse(CommonNounsConstant.REQUEST_CONTENT_TYPE), JSONObject.toJSONString(body)))
                .build();
    }

    /**
     * Bian NONE GET 测试 请求体封装
     * @param uri 接口路径
     * @param queryObject get请求入参，必须使用实体类传参，且属性名必须与接口文档中一致
     * @return
     */
    public Request getRequestBianForGetMN(String uri, Object queryObject) {
        return new Request.Builder()
                .url(ExchangeEnum.EXCHANGE_BINANCE_TEST.getHost() + uri +"?"+FormatUtils.formatQueryParamsForGet(queryObject))
                .get()
                .build();
    }

    /**
     * Bian NONE POST 测试 请求体封装
     * @param uri 接口路径
     * @param body 请求入参，必须使用实体类传参，且属性名必须与接口文档中一致
     * @return
     */
    public Request getRequestBianForPostMN(String uri, Object body) {
        return new Request.Builder()
                .url(ExchangeEnum.EXCHANGE_BINANCE_TEST.getHost() + uri)
                .post(RequestBody.create(MediaType.parse(CommonNounsConstant.REQUEST_CONTENT_TYPE), JSONObject.toJSONString(body)))
                .build();
    }

    /**
     * BIAN连接服务器专用请求体包装--get
     * @param ownerId 账户持有者id
     * @param uri 接口路径
     * @param params 参数 必须使用实体类传参，且属性名必须与接口文档中一致
     * @return
     */
    public Request getApiAndSignBianHasHeaderRequestForGet(String uri, String ownerId, Object params) {
        TApiAccount TApiAccount = Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_BINANCE.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
        return new Request.Builder()
                .url(ExchangeEnum.EXCHANGE_BINANCE.getHost()+uri+"?"+ FormatUtils.formatQueryParamsForGet(BinanceSignatureUtils.sign(params, TApiAccount.getSecretKey())))
                .addHeader(RequestHeadersConstant.BIAN_APP_KAY, TApiAccount.getApiKey())
                .get()
                .build();
    }

    /**
     * BIAN连接服务器专用请求体包装--post
     * @param ownerId 账户持有者id
     * @param uri 接口路径
     * @param params 参数 必须使用实体类传参，且属性名必须与接口文档中一致
     * @return
     */
    public Request getApiAndSignBianHasHeaderRequestForPost(String uri, String ownerId, Object params) {
        TApiAccount TApiAccount = Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_BINANCE.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
        return new Request.Builder()
                .url(ExchangeEnum.EXCHANGE_BINANCE.getHost()+uri)
                .addHeader(RequestHeadersConstant.BIAN_APP_KAY, TApiAccount.getApiKey())
                .post(RequestBody.create(MediaType.parse(CommonNounsConstant.REQUEST_CONTENT_TYPE), JSON.toJSONString(BinanceSignatureUtils.sign(params, TApiAccount.getSecretKey()))))
                .build();
    }

    /**
     * BIAN连接测试服务器专用请求体包装--get
     * @param ownerId 账户持有者id
     * @param uri 接口路径
     * @param params 参数 必须使用实体类传参，且属性名必须与接口文档中一致
     * @return
     */
    public Request getApiAndSignBianHasHeaderRequestForGetMN(String uri, String ownerId, Object params) {
        TApiAccount TApiAccount = Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_BINANCE_TEST.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
        return new Request.Builder()
                .url(ExchangeEnum.EXCHANGE_BINANCE_TEST.getHost()+uri+"?"+ FormatUtils.formatQueryParamsForGet(BinanceSignatureUtils.sign(params, TApiAccount.getSecretKey())))
                .addHeader(RequestHeadersConstant.BIAN_APP_KAY, TApiAccount.getApiKey())
                .get()
                .build();
    }

    /**
     * BIAN连接测试服务器专用请求体包装--post
     * @param ownerId 账户持有者id
     * @param uri 接口路径
     * @param params 参数 必须使用实体类传参，且属性名必须与接口文档中一致
     * @return
     */
    public Request getApiAndSignBianHasHeaderRequestForPostMN(String uri, String ownerId, Object params) {
        TApiAccount TApiAccount = Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_BINANCE_TEST.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
        return new Request.Builder()
                .url(ExchangeEnum.EXCHANGE_BINANCE_TEST.getHost()+uri)
                .addHeader(RequestHeadersConstant.BIAN_APP_KAY, TApiAccount.getApiKey())
                .post(RequestBody.create(MediaType.parse(CommonNounsConstant.REQUEST_CONTENT_TYPE), JSON.toJSONString(BinanceSignatureUtils.sign(params, TApiAccount.getSecretKey()))))
                .build();
    }


    /**
     * BIAN连接服务器专用请求体包装--get
     * @param ownerId 账户持有者id
     * @param uri 接口路径
     * @param params 参数 必须使用实体类传参，且属性名必须与接口文档中一致
     * @return
     */
    public Request getApiBianHasHeaderRequestForGet(String uri, String ownerId, Object params) {
        TApiAccount TApiAccount = Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_BINANCE.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
        return new Request.Builder()
                .url(ExchangeEnum.EXCHANGE_BINANCE.getHost()+uri+"?"+ FormatUtils.formatQueryParamsForGet(params))
                .addHeader(RequestHeadersConstant.BIAN_APP_KAY, TApiAccount.getApiKey())
                .get()
                .build();
    }

    /**
     * BIAN连接服务器专用请求体包装--post
     * @param ownerId 账户持有者id
     * @param uri 接口路径
     * @param params 参数 必须使用实体类传参，且属性名必须与接口文档中一致
     * @return
     */
    public Request getApiBianHasHeaderRequestForPost(String uri, String ownerId, Object params) {
        TApiAccount TApiAccount = Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_BINANCE.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
        return new Request.Builder()
                .url(ExchangeEnum.EXCHANGE_BINANCE.getHost()+uri)
                .addHeader(RequestHeadersConstant.BIAN_APP_KAY, TApiAccount.getApiKey())
                .post(RequestBody.create(MediaType.parse(CommonNounsConstant.REQUEST_CONTENT_TYPE), JSON.toJSONString(params)))
                .build();
    }

    /**
     * BIAN连接服务器专用请求体包装--get
     * @param ownerId 账户持有者id
     * @param uri 接口路径
     * @param params 参数 必须使用实体类传参，且属性名必须与接口文档中一致
     * @return
     */
    public Request getApiBianHasHeaderRequestForGetMN(String uri, String ownerId, Object params) {
        TApiAccount TApiAccount = Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_BINANCE.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
        return new Request.Builder()
                .url(ExchangeEnum.EXCHANGE_BINANCE_TEST.getHost()+uri+"?"+ FormatUtils.formatQueryParamsForGet(params))
                .addHeader(RequestHeadersConstant.BIAN_APP_KAY, TApiAccount.getApiKey())
                .get()
                .build();
    }

    /**
     * BIAN连接服务器专用请求体包装--post
     * @param ownerId 账户持有者id
     * @param uri 接口路径
     * @param params 参数 必须使用实体类传参，且属性名必须与接口文档中一致
     * @return
     */
    public Request getApiBianHasHeaderRequestForPostMN(String uri, String ownerId, Object params) {
        TApiAccount TApiAccount = Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_BINANCE.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
        return new Request.Builder()
                .url(ExchangeEnum.EXCHANGE_BINANCE_TEST.getHost()+uri)
                .addHeader(RequestHeadersConstant.BIAN_APP_KAY, TApiAccount.getApiKey())
                .post(RequestBody.create(MediaType.parse(CommonNounsConstant.REQUEST_CONTENT_TYPE), JSON.toJSONString(params)))
                .build();
    }

    /**
     * FTX连接服务器专用请求体包装--get
     * @param uri 接口路径
     * @param queryParams get请求入参，必须使用实体类传参，且属性名必须与接口文档中一致
     * @param ownerId 账户持有者id
     * @param subAccount 要使用的子帐户的URI编码名称，若没有，请传 null
     * @return
     */
    public Request getFtxRequestForGet(String uri, Object queryParams, String ownerId, String subAccount) {
        String time = Long.toString(System.currentTimeMillis());
        TApiAccount TApiAccount = Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_FTX.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
        if(!ObjectUtils.isEmpty(queryParams)) {
            // 得到类对象
            Class userCla = (Class) queryParams.getClass();
            /* 得到类中的所有属性集合 */
            Field[] fs = userCla.getDeclaredFields();
            for (int i = 0; i < fs.length; i++) {
                Field f = fs[i];
                // 设置些属性是可以访问的
                f.setAccessible(true);
                Object val = new Object();
                try {
                    // 得到此属性的值
                    val = f.get(queryParams);
                    // 设置键值
                    if(!ObjectUtils.isEmpty(val)) {
                        uri.replace(f.getName(), val.toString());
                    }
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        if(null != subAccount && !"".equals(subAccount)) {
            return new Request.Builder().addHeader(RequestHeadersConstant.FTX_SUBACCOUNT, subAccount)
                    .addHeader(RequestHeadersConstant.FTX_KEY, Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_FTX.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK).getApiKey()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_API_KEY_IS_EMPTY)))
                    .addHeader(RequestHeadersConstant.FTX_TS, time)
                    .addHeader(RequestHeadersConstant.FTX_SIGN, FtxSignatureUtils.sign(TApiAccount.getSecretKey(), time, "GET", uri, JSON.toJSONString(queryParams)))
                    .url(ExchangeEnum.EXCHANGE_FTX.getHost()+uri)
                    .get().build();
        }
        return new Request.Builder()
                .addHeader(RequestHeadersConstant.FTX_KEY, Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_FTX.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK).getApiKey()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_API_KEY_IS_EMPTY)))
                .addHeader(RequestHeadersConstant.FTX_TS, time)
                .addHeader(RequestHeadersConstant.FTX_SIGN, FtxSignatureUtils.sign(TApiAccount.getSecretKey(), time, "GET", uri, JSON.toJSONString(queryParams)))
                .url(ExchangeEnum.EXCHANGE_FTX.getHost()+uri)
                .get().build();
    }


    /**
     * FTX连接服务器专用请求体包装--post
     * @param uri 接口路径
     * @param params 请求体（JSON字符串）
     * @param ownerId 账户持有者id
     * @param subAccount 要使用的子帐户的URI编码名称，若没有，请传 null
     * @return
     */
    public Request getFTXRequestForPost(String uri, String params, String ownerId, String subAccount) {
        String time = Long.toString(System.currentTimeMillis());
        TApiAccount TApiAccount = Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_FTX.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
        if(null != subAccount && !"".equals(subAccount)) {
            return new Request.Builder().addHeader(RequestHeadersConstant.FTX_SUBACCOUNT, subAccount)
                    .addHeader(RequestHeadersConstant.FTX_KEY, Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_FTX.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK).getApiKey()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_API_KEY_IS_EMPTY)))
                    .addHeader(RequestHeadersConstant.FTX_TS, time)
                    .addHeader(RequestHeadersConstant.FTX_SIGN, FtxSignatureUtils.sign(TApiAccount.getSecretKey(), time, "POST", uri, params))
                    .url(ExchangeEnum.EXCHANGE_FTX.getHost()+uri)
                    .post(RequestBody.create(MediaType.parse(CommonNounsConstant.REQUEST_CONTENT_TYPE), params))
                    .build();
        }
        return new Request.Builder()
                .addHeader(RequestHeadersConstant.FTX_KEY, Optional.ofNullable(getAccount(ownerId, ExchangeEnum.EXCHANGE_FTX.getCode(), CommonNounsConstant.EXCHANGEID_TAG_REAL_DISK).getApiKey()).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_API_KEY_IS_EMPTY)))
                .addHeader(RequestHeadersConstant.FTX_TS, time)
                .addHeader(RequestHeadersConstant.FTX_SIGN, FtxSignatureUtils.sign(TApiAccount.getSecretKey(), time, "POST", uri, params))
                .url(ExchangeEnum.EXCHANGE_FTX.getHost()+uri)
                .post(RequestBody.create(MediaType.parse(CommonNounsConstant.REQUEST_CONTENT_TYPE), params))
                .build();
    }

    private TApiAccount getAccount(String ownerId, Integer exchangeId, Integer tag) {
        return Optional.ofNullable(tApiAccountDAO.selectAccount(QueryAccountIDTO.builder().ownerId(ownerId).exchangeId(exchangeId).tag(tag).build()).get(0)).orElseThrow(() -> new MyException(ReturnMsg.THE_USER_ACCOUNT_INFO_IS_EMPTY));
    }


    //由于公司电脑连接vpn导致数据库不能用，所以获取账户信息特用此方法代替
    //todo 替换
//    private TApiAccount getAccount(String ownerId, Integer exchangeId, Integer tag) {
//        TApiAccount TApiAccount = new TApiAccount();
//        switch (exchangeId) {
//            case 1:
//                if(tag == 1) {
//                    TApiAccount.setApiKey("5eb7b056-9fb5-41d0-988f-868b0cc7fc1e");
//                    TApiAccount.setSecretKey("C082553C55657947C209F8EEB2CCB8A4");
//                    TApiAccount.setPassPhrase("7013572736asd");
//                    return TApiAccount;
//                }
//                TApiAccount.setApiKey("85e03641-0eb9-45c5-a122-dfed228e81ef");
//                TApiAccount.setSecretKey("A3FE8862A00BFC62EF5E472D72E131FF");
//                TApiAccount.setPassPhrase("7013572736asd");
//                return TApiAccount;
//            case 2:
//                TApiAccount.setApiKey("1af1ae2b-bg5t6ygr6y-3835a4cc-ee3ef");
//                TApiAccount.setSecretKey("e9d7fe9e-a89a15f2-2dc68993-65165");
//                TApiAccount.setPassPhrase("166950122");
//                return TApiAccount;
//            case 3:
//                TApiAccount.setApiKey("dlrCUNHrTkd013OlXQGD2EybVMTmfCRBkWLMwha6b3SYgIbNGdKiMd0AlmxqS2nm");
//                TApiAccount.setSecretKey("eE69YJHm4uaoaDRoqDJkpQuAwVVTO93mwJxdeQNGG2Cnrn6OhkLCBkOUmkG1Y31F");
//                return TApiAccount;
//            case 4:
//                TApiAccount.setApiKey("A_E5IFCfpp6wj2KkEhwWdiBQ8nL9l4cLTMsUTU6R");
//                TApiAccount.setSecretKey("gK490q2ryShXzOeTizgETPwR1SN_QvikX8jVHYMZ");
//                return TApiAccount;
//        }
//        return TApiAccount;
//    }
}
