package com.lhjy.quantitativetrading.utils.signature;

import com.lhjy.quantitativetrading.dal.constant.CommonNounsConstant;
import com.lhjy.quantitativetrading.utils.common.MyException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;

/**
 * @ClassName BinanceSignatureUtils
 * @Description FTX鉴权工具类
 * @Author wangdehonga
 * @Date 2020/10/22 13:50
 * @Version 1.0
 */
public class FtxSignatureUtils {

    /**
     *
     * @param secretKey API secret key
     * @param time Unix时间戳
     * @param method "POST" or "GET" or "DELETE" or "PUT"
     * @param uri 接口路径
     * @param params 请求参数（JSON字符串）
     * @return String
     */
    public static String sign(String secretKey, String time, String method, String uri, String params) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(time).append(method.toUpperCase()).append("/api").append(uri);
        if(null != params && !"".equals(params)) {
            stringBuilder.append(params);
        }
        return toSha256_HMAC(secretKey, stringBuilder.toString());
    }

    public  static String toSha256_HMAC(String secretKey, String msg) {
        String hash = "";
        try {
            Mac sha256_HMAC = Mac.getInstance(CommonNounsConstant.HMAC_SHA256);
            SecretKeySpec secret_key = new SecretKeySpec(secretKey.getBytes(), CommonNounsConstant.HMAC_SHA256);
            sha256_HMAC.init(secret_key);
            byte[] bytes = sha256_HMAC.doFinal(msg.getBytes());
            hash = byteArrayToHexString(bytes);
        } catch (Exception e) {
            throw new MyException(-9999, "Error HmacSHA256 ===========" + e.getMessage());
        }
        return hash;
    }

    public  static String byteArrayToHexString(byte[] b) {
        StringBuilder hs = new StringBuilder();
        String stmp;
        for (int n = 0; b!=null && n < b.length; n++) {
            stmp = Integer.toHexString(b[n] & 0XFF);
            if (stmp.length() == 1) {
                hs.append('0');
            }
            hs.append(stmp);
        }
        return hs.toString().toLowerCase();
    }
}
