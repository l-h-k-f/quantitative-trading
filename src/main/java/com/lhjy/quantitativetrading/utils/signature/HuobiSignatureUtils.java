package com.lhjy.quantitativetrading.utils.signature;

import com.lhjy.quantitativetrading.dal.cenum.ExchangeEnum;
import com.lhjy.quantitativetrading.dal.constant.CommonNounsConstant;
import com.lhjy.quantitativetrading.dal.constant.RequestHeadersConstant;
import com.lhjy.quantitativetrading.utils.common.MyException;
import org.springframework.util.CollectionUtils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @ClassName HuobiSignatureUtils
 * @Description 火币鉴权及url拼接工具类
 * @Author wangdehonga
 * @Date 2020/10/22 12:31
 * @Version 1.0
 */
public class HuobiSignatureUtils {

    /**
     * @param timeStamp
     * @param accessKey
     * @param secretKey
     * @param method get or post
     * @param uri 接口路径
     * @param params 请求参数 对于 POST 请求，每个方法自带的参数不进行签名认证，并且需要放在 body 中
     * @return
     * @throws Exception
     */
    public static String signAndUrl(String timeStamp, String accessKey, String secretKey, String method,
                              String uri, Map<String, String> params) throws Exception {
        StringBuilder sb = new StringBuilder(1024);
        if (accessKey == null || "".equals(accessKey) || secretKey == null || "".equals(secretKey)) {
            throw new MyException("accessKey or secretKey is empty! ");
        }
        sb.append(method.toUpperCase()).append('\n')
                .append(ExchangeEnum.EXCHANGE_HUOBI.getHost().toLowerCase()).append('\n')
                .append(uri).append('\n');
        if(CollectionUtils.isEmpty(params)) {
            params = new HashMap<>(5);
        }
        params.put(RequestHeadersConstant.ACCESS_KEY_ID, accessKey);
        params.put(RequestHeadersConstant.SIGNATURE_VERSION, RequestHeadersConstant.SIGNATURE_VERSION_VALUE);
        params.put(RequestHeadersConstant.SIGNATURE_METHOD, RequestHeadersConstant.SIGNATURE_METHOD_VALUE);
        params.put(RequestHeadersConstant.TIMESTAMP, timeStamp);

        sb.append(appendUrl(params));

        Mac hmacSha256;
        try {
            hmacSha256 = Mac.getInstance(RequestHeadersConstant.SIGNATURE_METHOD_VALUE);
            SecretKeySpec secKey = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8),
                    RequestHeadersConstant.SIGNATURE_METHOD_VALUE);
            hmacSha256.init(secKey);
        } catch (NoSuchAlgorithmException e) {
            throw new MyException("[Signature] No such algorithm: " + e.getMessage());
        } catch (InvalidKeyException e) {
            throw new MyException("[Signature] Invalid key: " + e.getMessage());
        }
        String payload = sb.toString();
        byte[] hash = hmacSha256.doFinal(payload.getBytes(StandardCharsets.UTF_8));
        String actualSign = Base64.getEncoder().encodeToString(hash);
        return ExchangeEnum.EXCHANGE_HUOBI.getHost()+uri+"?"+appendUrl(params)+"&"+RequestHeadersConstant.SIGNATURE+"="+actualSign;
    }

    private static String appendUrl(Map<String, String> params) throws Exception {
        Map<String, String> map = new TreeMap<>(params);
        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (!("").equals(stringBuilder.toString())) {
                stringBuilder.append("&");
            }
            stringBuilder.append(entry.getKey());
            stringBuilder.append("=");
            stringBuilder.append(urlEncode(entry.getValue()));
        }
        return stringBuilder.toString();
    }

    /**
     * 使用标准URL Encode编码。注意和JDK默认的不同，空格被编码为%20而不是+。
     *
     * @param s String字符串
     * @return URL编码后的字符串
     */
    private static String urlEncode(String s) throws Exception {
        try {
            return URLEncoder.encode(s, CommonNounsConstant.UTF_8_NAME).replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            throw new MyException("[URL] UTF-8 encoding not supported!");
        }
    }
}
