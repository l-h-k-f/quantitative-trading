package com.lhjy.quantitativetrading.utils.signature;

import com.lhjy.quantitativetrading.dal.constant.CommonNounsConstant;
import com.lhjy.quantitativetrading.utils.common.MyException;
import org.apache.commons.lang3.StringUtils;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.management.RuntimeErrorException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * @ClassName OkexSignature
 * @Description okex签名认证, timestamp + method + requestPath + body字符串(+表示字符串连接)，
 * 以及SecretKey，使用HMAC SHA256方法加密，通过Base64编码输出而得到的;
 * @Author wangdehonga
 * @Date 2020/10/21 19:35
 * @Version 1.0
 */
public class OkexSignatureUtils {

    public static Mac MAC;

    static {
        try {
            MAC = Mac.getInstance(CommonNounsConstant.HMAC_SHA256);
        } catch (NoSuchAlgorithmException e) {
            throw new MyException(-9999, "Can't get Mac's instance.");
        }
    }


    /**
     * Okex签名函数
     * @param timestamp 时间戳，eg: 2020-03-28T12:21:581Z(rest) / 1585403514.843(websocket)
     * @param method 请求类型，eg：POST
     * @param requestPath 请求路径，eg：/api/general/v3/time
     * @param queryString 请求入参 eg：type=2&currency=btc&after=9260348&limit=10
     * @param body 请求体
     * @param secretKey secretKey
     * @return String
     * @throws CloneNotSupportedException
     * @throws InvalidKeyException
     * @throws UnsupportedEncodingException
     */
    public static String sign(String timestamp, String method, String requestPath,
                              String queryString, String body, String secretKey)
            throws CloneNotSupportedException, InvalidKeyException, UnsupportedEncodingException {
        if (StringUtils.isEmpty(secretKey) || StringUtils.isEmpty(method)) {
            return "";
        }

        String preHash = preHash(timestamp, method, requestPath, queryString, body);
        byte[] secretKeyBytes = secretKey.getBytes(CommonNounsConstant.UTF_8_NAME);
        SecretKeySpec secretKeySpec = new SecretKeySpec(secretKeyBytes, CommonNounsConstant.HMAC_SHA256);
        Mac mac = (Mac) MAC.clone();
        mac.init(secretKeySpec);
        return Base64.getEncoder().encodeToString(mac.doFinal(preHash.getBytes(CommonNounsConstant.UTF_8_NAME)));
    }


    public static String preHash(String timestamp, String method, String requestPath,
                                 String queryString, String body) {
        StringBuilder preHash = new StringBuilder();
        preHash.append(timestamp);
        preHash.append(method.toUpperCase());
        preHash.append(requestPath);
        //get方法
        if (StringUtils.isNotEmpty(queryString)) {
            //在queryString前面拼接上？
            preHash.append("?").append(queryString);
        }
        //post方法
        if (StringUtils.isNotEmpty(body)) {
            preHash.append(body);
        }
        return preHash.toString();
    }
}
