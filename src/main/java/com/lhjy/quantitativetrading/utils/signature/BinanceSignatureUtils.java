package com.lhjy.quantitativetrading.utils.signature;

import com.lhjy.quantitativetrading.dal.constant.RequestHeadersConstant;
import com.lhjy.quantitativetrading.utils.common.FormatUtils;
import com.lhjy.quantitativetrading.utils.common.MyException;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.security.InvalidKeyException;
import java.util.Map;

/**
 * @ClassName BinanceSignatureUtils
 * @Description 币安鉴权工具类
 * @Author wangdehonga
 * @Date 2020/10/22 13:50
 * @Version 1.0
 */
public class BinanceSignatureUtils {

    public static Object sign(Object object, String secretKey) {
        try {
            // 获取obj类的字节文件对象、该类的成员变量
            Field f = object.getClass().getDeclaredField("signature");
            // 取消语言访问检查
            f.setAccessible(true);
            // 给变量赋值
            f.set(object, FtxSignatureUtils.toSha256_HMAC(secretKey, FormatUtils.formatQueryParamsForGet(object)));
            return object;
        } catch (Exception e) {
            throw new MyException("对象属性解析异常" + e.getMessage());
        }
    }
}
