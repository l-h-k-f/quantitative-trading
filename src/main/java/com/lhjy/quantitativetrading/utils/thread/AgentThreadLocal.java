package com.lhjy.quantitativetrading.utils.thread;

import com.lhjy.quantitativetrading.dal.entity.UserInfo;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @ProjectName: wdkf-spring-boot-starter
 * @Package: com.wdkf.wdkfspringbootautoconfigure.utils.thread
 * @ClassName: AgentThreadLocal
 * @Description: 本地线程交互类
 * @Author: wangdehonga
 * @Date: 2020/7/29 17:11
 * @Version: 1.0
 */
@Component
public class AgentThreadLocal {

    private AgentThreadLocal() {
    }

    /**
     * 用户信息
     */
    private static final ThreadLocal<UserInfo> LOCAL = new ThreadLocal<UserInfo>();

    public static void set(UserInfo user) {
        LOCAL.set(user);
    }

    public static UserInfo get() {
        return LOCAL.get();
    }

    public static void remove() {
        LOCAL.remove();
    }
}
