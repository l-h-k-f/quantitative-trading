package com.lhjy.quantitativetrading.utils.date;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

/**
 * @description:
 * @Author Dehong Wang
 * @Date 20-10-21 23:46
 * @Version 1.0
 */
public class DateUtils {

    /**
     * 获取UTC时间 2020-03-28T12:21:41.274Z
     */
    public static String getUtcDateForOkex() {
        StringBuffer utcStr = new StringBuffer();
        //取得本地时间
        Calendar calendar = Calendar.getInstance();
        //时间偏移量
        int zoneOffSet = calendar.get(Calendar.ZONE_OFFSET);
        //夏令时差
        int dstOffSet = calendar.get(Calendar.DST_OFFSET);
        //在本地时间扣除差量
        calendar.add(Calendar.MILLISECOND, -(zoneOffSet + dstOffSet));
        ZoneId ZONE_GMT = ZoneId.of("Z");
        DateTimeFormatter DT_FORMAT = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss");
        return Instant.ofEpochSecond(Instant.now().getEpochSecond()).atZone(ZONE_GMT).format(DT_FORMAT)+"."+calendar.get(Calendar.MILLISECOND)+"Z";
    }

    /**
     * 获取UTC时间 2020-03-28T12:21:41
     */
    public static String getUtcDateForHuoBi() {
        ZoneId ZONE_GMT = ZoneId.of("Z");
        DateTimeFormatter DT_FORMAT = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss");
        return Instant.ofEpochSecond(Instant.now().getEpochSecond()).atZone(ZONE_GMT).format(DT_FORMAT);
    }
}
