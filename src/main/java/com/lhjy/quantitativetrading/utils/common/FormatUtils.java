package com.lhjy.quantitativetrading.utils.common;

import org.springframework.util.ObjectUtils;

import java.lang.reflect.Field;

/**
 * @description: 格式化工具
 * @Author Dehong Wang
 * @Date 20-10-24 14:49
 * @Version 1.0
 */
public class FormatUtils {

    /**
     * 格式化GET请求入参
     * @param object 实体类
     * @return a=1&b=2
     */
    public static String formatQueryParamsForGet(Object object) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Field f : object.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            try {
                Object value = f.get(object);
                //判断字段是否为空，并且对象属性中的基本都会转为对象类型来判断
                if (!ObjectUtils.isEmpty(value)) {
                    if (!("").equals(stringBuilder.toString())) {
                        stringBuilder.append("&");
                    }
                    stringBuilder.append(f.getName());
                    stringBuilder.append("=");
                    stringBuilder.append(value);
                }
            } catch (Exception e) {
                throw new MyException("对象属性解析异常" + e.getMessage());
            }
        }
        return stringBuilder.toString();
    }
}
