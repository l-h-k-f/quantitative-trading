package com.lhjy.quantitativetrading.utils.common;

import com.lhjy.quantitativetrading.dal.entity.UserInfo;
import com.lhjy.quantitativetrading.utils.thread.AgentThreadLocal;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.Set;

/**
 * @ProjectName: frist_dome
 * @Package: com.wdkf.wdkfspringbootautoconfigure.utils
 * @ClassName: LogRecordAspect
 * @Description: 切面
 * @Author: wangdehonga
 * @Date: 2020/7/5 10:28
 * @Version: 1.0
 */
@Slf4j
@Configuration
@Aspect
@Component
@Order(1)
public class RequestInfoAspect {
    @Autowired
    private RedisUtil redisUtil;
    /**
     * 定义切点Pointcut
     */
    @Pointcut("execution(* com.*..controller.*.*.*(..)) &&!@annotation(com.lhjy.quantitativetrading.utils.customannotation.NoAuthentication)")
    public void excudeService() {
    }


    @Around("excudeService()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {

        //获取请求
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();

        //获取请求头的authcode
        Enumeration<String> enumeration = request.getHeaderNames();
        String auth = null;
        while (enumeration.hasMoreElements()) {

            if ("token".equals(enumeration.nextElement())) {
                auth = "".equals(request.getHeader("token")) ? null : request.getHeader("token");
                break;
            }
        }
        //获取操作人信息
        if(null == auth || "".equals(auth)) {
            throw new MyException(ReturnMsg.THE_USER_TOKEN_IS_BE_OVERDUE);
        }
        Set<String> keys = redisUtil.getKeysList(auth);
        if(CollectionUtils.isEmpty(keys)) {
            throw new MyException(ReturnMsg.THE_USER_TOKEN_IS_BE_OVERDUE);
        }
        Object[] objects = keys.toArray();
        String token = (String) objects[0];
        if(!auth.equals(token)) {
            throw new MyException(ReturnMsg.THE_USER_TOKEN_IS_BE_OVERDUE);
        }
        //保存用户信息
        AgentThreadLocal.set((UserInfo) redisUtil.get(auth));
        //更新缓存失效时间
        redisUtil.expire(auth, 1800);
        //调用方法
        Object result = pjp.proceed();

        return result;
    }

}
