package com.lhjy.quantitativetrading.utils.common;

import java.util.Random;

/**
 * @description:
 * @Author Dehong Wang
 * @Date 20-10-29 22:29
 * @Version 1.0
 */
public class RandomUtils {

    /**
     * @Method: randomNumbers
     * @Description: 随机生成纯数字（指定长度）
     * flag：是否允许重复；true允许重复
     * @param length
     * @Return: java.lang.String
     * @Author: 王品峰
     * @Date 2020/8/27 14:40
     * @Version:  1.0
     */
    public static String randomNumbers(Integer length) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < length; i++) {
            sb.append(random.nextInt(10));
        }
        return sb.toString();
    }

}
