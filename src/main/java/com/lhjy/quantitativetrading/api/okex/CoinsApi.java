package com.lhjy.quantitativetrading.api.okex;

import com.alibaba.fastjson.JSON;
import com.lhjy.quantitativetrading.dal.query.okex.GetKLineDateQuery;
import com.lhjy.quantitativetrading.dal.query.okex.fund.OrdersQuery;
import com.lhjy.quantitativetrading.dal.result.okex.GetKLineDateResult;
import com.lhjy.quantitativetrading.dal.result.okex.TickerInfoResult;
import com.lhjy.quantitativetrading.dal.result.okex.fundaccount.OrdersResult;
import com.lhjy.quantitativetrading.utils.common.MyException;
import com.lhjy.quantitativetrading.utils.http.HttpUtils;
import com.lhjy.quantitativetrading.utils.http.RequestUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @description: 币币相关接口
 * @Author Dehong Wang
 * @Date 20-10-24 14:39
 * @Version 1.0
 */
@Slf4j
@Component
public class CoinsApi {

    /**
     * 获取K线数据
     */
    private static final String K_LINE_URI = "/api/spot/v3/instruments/<instrument_id>/candles";

    /**
     * 获取某个ticker信息
     */
    private static final String SOMEONE_TICKER_URI = "/api/spot/v3/instruments/<instrument-id>/ticker";

    /**
     * 下单
     */
    private static final String PLACE_AN_ORDER = "/api/spot/v3/orders";

    @Autowired
    RequestUtils requestUtils;
    /**
     * 获取币对的K线数据。K线数据按请求的粒度分组返回，K线数据最多可获取最近1440条。
     * @param getKLineDateQuery 获取K线数据查询参数
     * @param ownerId 账户持有者id
     * @return List<GetKLineDateResult>
     */
    public List<GetKLineDateResult> getKLineDate(GetKLineDateQuery getKLineDateQuery, String ownerId) {
        try {
            // todo 该获取请求体为模拟盘请求体
            String result = HttpUtils.execute(requestUtils.getOkexRequestForGetMN(K_LINE_URI.replace("<instrument_id>", getKLineDateQuery.getInstrument_id()+""), getKLineDateQuery, ownerId));
            List<GetKLineDateResult> getKLineDateResults = new ArrayList<>();
            if(StringUtils.isNotEmpty(result)) {
                Arrays.stream(result.substring(2, result.length()-2).split("],\\[")).forEach(list -> {
                    GetKLineDateResult getKLineDateResult = new GetKLineDateResult();
                    String[] subs = list.split(",");
                    getKLineDateResult.setTime(subs[0].substring(1,subs[0].length()-1));
                    getKLineDateResult.setOpen(subs[1].substring(1,subs[1].length()-1));
                    getKLineDateResult.setHigh(subs[2].substring(1,subs[2].length()-1));
                    getKLineDateResult.setLow(subs[3].substring(1,subs[3].length()-1));
                    getKLineDateResult.setClose(subs[4].substring(1,subs[4].length()-1));
                    getKLineDateResult.setVolume(subs[5].substring(1,subs[5].length()-1));
                    getKLineDateResults.add(getKLineDateResult);
                });
            }
            return getKLineDateResults;
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
    }

    /**
     * 获取币对的最新成交价、买一价、卖一价和24小时交易量的快照信息
     * @param instrument_id 币对
     * @param ownerId 账户持有者id
     * @return List<TickerInfoResult>
     */
    public TickerInfoResult getTickerInfoOf24h(String instrument_id, String ownerId) {
        try {
            // todo 该获取请求体为模拟盘请求体
            return JSON.parseObject(HttpUtils.execute(requestUtils.getOkexRequestForGetMN(SOMEONE_TICKER_URI.replace("<instrument-id>",
                    instrument_id+""), null, ownerId)), TickerInfoResult.class);
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
    }

    /**
     * 下单（模拟）
     * @param ordersQuery 下单
     * @param ownerId 账户持有者id
     * @return OrdersResult
     */
    public OrdersResult placeOrderMN(OrdersQuery ordersQuery, String ownerId) {
        try {
            return JSON.parseObject(HttpUtils.execute(requestUtils.getOkexRequestForPostMN(PLACE_AN_ORDER, ordersQuery, ownerId)), OrdersResult.class);
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
    }

    /**
     * 下单
     * @param ordersQuery 下单
     * @param ownerId 账户持有者id
     * @return OrdersResult
     */
    public OrdersResult placeOrder(OrdersQuery ordersQuery, String ownerId) {
        try {
            return JSON.parseObject(HttpUtils.execute(requestUtils.getOkexRequestForPost(PLACE_AN_ORDER, ordersQuery, ownerId)), OrdersResult.class);
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
    }
}
