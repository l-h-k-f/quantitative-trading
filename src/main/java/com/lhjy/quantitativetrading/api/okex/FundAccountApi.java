package com.lhjy.quantitativetrading.api.okex;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lhjy.quantitativetrading.dal.query.okex.fund.OrdersQuery;
import com.lhjy.quantitativetrading.dal.query.okex.fund.TransferQuery;
import com.lhjy.quantitativetrading.dal.result.okex.fundaccount.OrdersResult;
import com.lhjy.quantitativetrading.dal.result.okex.fundaccount.TransferResult;
import com.lhjy.quantitativetrading.dal.result.okex.fundaccount.WalletResult;
import com.lhjy.quantitativetrading.utils.common.MyException;
import com.lhjy.quantitativetrading.utils.http.HttpUtils;
import com.lhjy.quantitativetrading.utils.http.RequestUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @description: 资金账户相关api
 * @Author Dehong Wang
 * @Date 20-10-22 20:59
 * @Version 1.0
 */
@Slf4j
@Component
public class FundAccountApi {

    /**
     * 获取资金账户信息
     */
    private static final String FUND_ACCOUNT = "/api/account/v3/wallet";

    /**
     * 获取单一币种账户信息
     */
    private static final String SINGLE_CURRENCY_ACCOUNT = "/api/account/v3/wallet/<currency>";

    /**
     * 资金划转
     */
    private static final String FUND_TRANSFER = "/api/account/v3/transfer";

    @Autowired
    RequestUtils requestUtils;

    /**
     * 获取资金账户所有资产列表，查询各币种的余额、冻结和可用等信息。（模拟）
     * @param ownerId  账户持有者id
     * @return List<WalletResult>
     */
    public List<WalletResult> getWalletMN(String ownerId) {
        try {
            return JSONObject.parseArray(HttpUtils.execute(requestUtils.getOkexRequestForGetMN(FUND_ACCOUNT, null, ownerId)), WalletResult.class);
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
    }

    /**
     * 获取资金账户所有资产列表，查询各币种的余额、冻结和可用等信息。
     * @param ownerId  账户持有者id
     * @return List<WalletResult>
     */
    public List<WalletResult> getWallet(String ownerId) {
        try {
            return JSONObject.parseArray(HttpUtils.execute(requestUtils.getOkexRequestForGet(FUND_ACCOUNT, null, ownerId)), WalletResult.class);
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
    }

    /**
     * 获取资金账户单个币种的余额、冻结和可用等信息。（模拟）
     * @param currency 币种
     * @param ownerId  账户持有者id
     * @return WalletResult
     */
    public WalletResult getWalletByCoinsMN(String currency, String ownerId) {
        try {
            return JSON.parseObject(HttpUtils.execute(requestUtils.getOkexRequestForGetMN(SINGLE_CURRENCY_ACCOUNT.replace("<currency>", currency), null, ownerId)), WalletResult.class);
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
    }

    /**
     * 获取资金账户单个币种的余额、冻结和可用等信息。
     * @param currency 币种
     * @param ownerId  账户持有者id
     * @return WalletResult
     */
    public WalletResult getWalletByCoins(String currency, String ownerId) {
        try {
            return JSON.parseObject(HttpUtils.execute(requestUtils.getOkexRequestForGet(SINGLE_CURRENCY_ACCOUNT.replace("<currency>", currency), null, ownerId)), WalletResult.class);
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
    }

    /**
     * OKEx站内在资金账户、交易账户和子账户之间进行资金划转。（模拟）
     * @param transferQuery 资金划转
     * @param ownerId 账户持有者id
     * @return TransferResult
     */
    public TransferResult TransferMN(TransferQuery transferQuery, String ownerId) {
        try {
            return JSON.parseObject(HttpUtils.execute(requestUtils.getOkexRequestForPostMN(FUND_TRANSFER, transferQuery, ownerId)), TransferResult.class);
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
    }

    /**
     * OKEx站内在资金账户、交易账户和子账户之间进行资金划转。
     * @param transferQuery 资金划转
     * @param ownerId 账户持有者id
     * @return TransferResult
     */
    public TransferResult Transfer(TransferQuery transferQuery, String ownerId) {
        try {
            return JSON.parseObject(HttpUtils.execute(requestUtils.getOkexRequestForPost(FUND_TRANSFER, transferQuery, ownerId)), TransferResult.class);
        } catch (Exception e) {
            throw new MyException(e.getMessage());
        }
    }
}
