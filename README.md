# quantitative-trading

量化开发

## 请求体封装问题

    1、OKEX 所有接口均需校验 （可模拟）
    | 请求类型 | 盘类 |                         获取请求体方法                               |      校验类型      |
    
    |   GET   | 实盘 |         getOkexRequestForGet (uri, queryParam, userId)              |    okex统一验签    |
    |   GET   | 模拟 |         getOkexRequestForGetMN (uri, queryParam, userId)            |    okex统一验签    |
    |   POST  | 实盘 |         getOkexRequestForPost (uri, body, userId)                   |    okex统一验签    |
    |   POST  | 模拟 |         getOkexRequestForPostMN (uri, body, userId)                 |    okex统一验签    |

    2、Binance 分三种请求情况（可模拟）
    |   GET   | 实盘 | getRequestBianForGet (uri, queryParam, userId)                      |        NONE       |
    |   GET   | 模拟 | getRequestBianForGetMN (uri, queryParam, userId)                    |        NONE       |
    |   POST  | 实盘 | getRequestBianForPost (uri, body, userId)                           |        NONE       |
    |   POST  | 模拟 | getRequestBianForPostMN (uri, body, userId)                         |        NONE       |
    |   GET   | 实盘 | getApiAndSignBianHasHeaderRequestForGet (uri, queryParam, userId)   |   API-Key 和签名   |
    |   GET   | 模拟 | getApiAndSignBianHasHeaderRequestForGetMN (uri, queryParam, userId) |   API-Key 和签名   |
    |   POST  | 实盘 | getApiAndSignBianHasHeaderRequestForPost (uri, body, userId)        |   API-Key 和签名   |
    |   POST  | 模拟 | getApiAndSignBianHasHeaderRequestForPostMN (uri, body, userId)      |   API-Key 和签名   |
    |   GET   | 实盘 | getApiBianHasHeaderRequestForGet (uri, queryParam, userId)          |       API-Key     |
    |   GET   | 模拟 | getApiBianHasHeaderRequestForGetMN (uri, queryParam, userId)        |       API-Key     |
    |   POST  | 实盘 | getApiBianHasHeaderRequestForPost (uri, body, userId)               |       API-Key     |
    |   POST  | 模拟 | getApiBianHasHeaderRequestForPostMN (uri, body, userId)             |       API-Key     |

    3、huobi 分两种请求情况   
    |   GET   |               getRequestHuoBiForGet (uri, queryParam)                     |        NONE        |       
    |   POST  |               getRequestHuoBiForPost (uri, body)                          |        NONE        |       
    |   GET   |               getSignRequestHuoBiForGet (uri, queryParam, userId)         |        签名        |
    |   POST  |               getSignRequestHuoBiForPost (uri, body, userId)              |        签名        |

    4、FTX 所有接口均需校验
    
    |   GET   |                   getFtxRequestForGet (uri, queryParam, userId)           |     FTX统一验签    |
    |   POST  |                   getFTXRequestForPost (uri, body, userId)                |     FTX统一验签    |
    
## 币种问题
    1、okex模拟盘币对前必须家伙是那个 MN，如 MNBTC-MNUSDT