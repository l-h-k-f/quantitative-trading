/*
Navicat MySQL Data Transfer

Source Server         : wdkf-mysql
Source Server Version : 50649
Source Host           : 10.19.76.169:3306
Source Database       : d_lhjy

Target Server Type    : MYSQL
Target Server Version : 50649
File Encoding         : 65001

Date: 2020-11-02 20:05:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_api_account
-- ----------------------------
DROP TABLE IF EXISTS `t_api_account`;
CREATE TABLE `t_api_account` (
                                 `id` int(11) NOT NULL AUTO_INCREMENT,
                                 `owner_id` varchar(10) NOT NULL COMMENT '账户持有者id',
                                 `exchange_id` varchar(10) NOT NULL COMMENT '交易所编号',
                                 `api_key` varchar(100) NOT NULL COMMENT 'apikey',
                                 `secret_key` varchar(100) NOT NULL COMMENT '密钥',
                                 `pass_phrase` varchar(30) DEFAULT NULL COMMENT 'okex的pass_phrase和火币的UID保留字段',
                                 `tag` int(2) NOT NULL COMMENT '操盘标志，1：实盘；2：模拟盘',
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='接口信息表';

-- ----------------------------
-- Records of t_api_account
-- ----------------------------
INSERT INTO `t_api_account` VALUES ('1', '88888888', '1', '5eb7b056-9fb5-41d0-988f-868b0cc7fc1e', 'C082553C55657947C209F8EEB2CCB8A4', '7013572736asd', '1');
INSERT INTO `t_api_account` VALUES ('2', '88888888', '1', '85e03641-0eb9-45c5-a122-dfed228e81ef', 'A3FE8862A00BFC62EF5E472D72E131FF', '7013572736asd', '2');
INSERT INTO `t_api_account` VALUES ('3', '88888888', '2', '1af1ae2b-bg5t6ygr6y-3835a4cc-ee3ef', 'e9d7fe9e-a89a15f2-2dc68993-65165', '166950122', '1');
INSERT INTO `t_api_account` VALUES ('4', '88888888', '3', 'dlrCUNHrTkd013OlXQGD2EybVMTmfCRBkWLMwha6b3SYgIbNGdKiMd0AlmxqS2nm', 'eE69YJHm4uaoaDRoqDJkpQuAwVVTO93mwJxdeQNGG2Cnrn6OhkLCBkOUmkG1Y31F', null, '1');
INSERT INTO `t_api_account` VALUES ('5', '88888888', '4', 'A_E5IFCfpp6wj2KkEhwWdiBQ8nL9l4cLTMsUTU6R', 'gK490q2ryShXzOeTizgETPwR1SN_QvikX8jVHYMZ', null, '1');
INSERT INTO `t_api_account` VALUES ('8', 'dsgfsafdfs', '1', '11', '123', '123', '2');

-- ----------------------------
-- Table structure for t_currency
-- ----------------------------
DROP TABLE IF EXISTS `t_currency`;
CREATE TABLE `t_currency` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `currency_name` varchar(20) NOT NULL COMMENT '币种名称',
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='币种';

-- ----------------------------
-- Records of t_currency
-- ----------------------------
INSERT INTO `t_currency` VALUES ('1', 'BTC');
INSERT INTO `t_currency` VALUES ('2', 'ETH');
INSERT INTO `t_currency` VALUES ('3', 'LTC');
INSERT INTO `t_currency` VALUES ('4', 'EOS');
INSERT INTO `t_currency` VALUES ('5', 'BCH');
INSERT INTO `t_currency` VALUES ('6', 'BSV');
INSERT INTO `t_currency` VALUES ('7', 'TRX');
INSERT INTO `t_currency` VALUES ('8', 'XRP');
INSERT INTO `t_currency` VALUES ('9', 'LINK');
INSERT INTO `t_currency` VALUES ('10', 'ETC');

-- ----------------------------
-- Table structure for t_customer_info
-- ----------------------------
DROP TABLE IF EXISTS `t_customer_info`;
CREATE TABLE `t_customer_info` (
                                   `id` int(11) NOT NULL AUTO_INCREMENT,
                                   `customer_id` varchar(10) NOT NULL COMMENT '客户id，随机生成且唯一',
                                   `customer_name` varchar(20) NOT NULL COMMENT '客户称谓',
                                   `createor` varchar(20) NOT NULL COMMENT '添加人',
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='客户信息表';

-- ----------------------------
-- Records of t_customer_info
-- ----------------------------

-- ----------------------------
-- Table structure for t_user_info
-- ----------------------------
DROP TABLE IF EXISTS `t_user_info`;
CREATE TABLE `t_user_info` (
                               `id` int(11) NOT NULL AUTO_INCREMENT,
                               `user_id` varchar(10) NOT NULL COMMENT '用户id，随机生成且唯一',
                               `user_account` varchar(20) NOT NULL COMMENT '用户账号',
                               `user_password` varchar(20) NOT NULL COMMENT '用户密码',
                               `user_role` int(5) NOT NULL DEFAULT '2' COMMENT '角色：1：管理员（可添加用户）2：用户（仅可添加交易所账号信息）',
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of t_user_info
-- ----------------------------
INSERT INTO `t_user_info` VALUES ('1', '88888888', 'Adnis', '666666', '1');
